﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace Hamal.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MahApps.Metro.Controls.MetroWindow
    {
        public static MainWindow m_Window = null;
        public Page m_CurrentPage = null;
        public MainPage m_MainPage = new MainPage();

        public MainWindow()
        {
            m_Window = this;
            InitializeComponent();
            LoginPage();
        }

        private void MetroWindow_Closed(Object a_Sender, EventArgs a_Args)
        {
            NetworkListener.Kill();
            Environment.Exit(0); // We are too lazy to stop all running threads, so for now we just kill the process.
        }

        public void MainPage()
        {
            Dispatcher.BeginInvoke(new ThreadStart(() =>
            {
                ResizeMode = ResizeMode.CanResize;
                PageContainer.Content = (m_CurrentPage = m_MainPage).Content;
                m_MainPage.Update();
                Title = "Hamal v0.0.1";
            }));
        }

        public void LoginPage()
        {
            Dispatcher.BeginInvoke(new ThreadStart(() =>
            {
                ResizeMode = ResizeMode.NoResize;
                PageContainer.Content = (m_CurrentPage = new LoginPanel()).Content;
                Title = "Hamal v0.0.1 - Please log in";
            }));
        }

        internal void LoginError(AuthenticationState a_Status)
        {
            if (m_CurrentPage is LoginPanel)
                ((LoginPanel)m_CurrentPage).LoginError(a_Status);
            else
            {
                LoginPage();
                while (!(m_CurrentPage is LoginPanel)) ;
                ((LoginPanel)m_CurrentPage).LoginError(a_Status);
            }
        }
    }
}
