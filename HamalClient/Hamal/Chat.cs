﻿using JSocket;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Hamal.Client
{
    public class Chat
    {
        public static Dictionary<String, ChatRoom> m_Rooms = new Dictionary<String, ChatRoom>();

        public static async Task<ChatRoom> CreateOrJoinRoom(String a_TargetUid, String a_Name, String a_Password, Boolean a_IsPrivate)
        {
            JSONPacket t_Answer = await NetworkListener.WaitForChatPacket("JOINCREATE_ANSWER", new JSONPacket("CHAT", new Dictionary<String, Object>()
            {
                { "COMMAND", "JOINCREATE" },
                { "TARGET", a_TargetUid },
                { "NAME", a_Name },
                { "PASSWORD", a_Password.ToBase64() },
                { "PRIVATE", Convert.ToInt32(a_IsPrivate) }
            }));

            if (t_Answer.Data["STATE"].Equals("FAILED"))
                return null;

            ChatRoom t_Room = SerializeRoom((Dictionary<String, Object>)t_Answer.Data["ROOM"]);

            if (m_Rooms.ContainsKey(t_Room.UID))
                m_Rooms[t_Room.UID] = t_Room;
            else
                m_Rooms.Add(t_Room.UID, t_Room);

            return t_Room;
        }

        public static async Task<List<ChatRoom>> GetChatRoomsForUser(Int64 a_DBID)
        {
            JSONPacket t_Answer = await NetworkListener.WaitForChatPacket("GET_ROOMS_FOR_USER_ANSWER", new JSONPacket("CHAT", new Dictionary<String, Object>()
            {
                { "COMMAND", "GET_ROOMS_FOR_USER" },
                { "USER_UUID", a_DBID }
            }));

            if (t_Answer.Data["STATE"].Equals("FAILED"))
                return null;

            return ((ArrayList)t_Answer.Data["ROOMS"]).Cast<Dictionary<String, Object>>().Select(x => SerializeRoom(x)).ToList();
        }

        public static async Task<Boolean> DoesChatRoomExist(String a_UID)
        {
            JSONPacket t_Answer = await NetworkListener.WaitForChatPacket("ROOM_EXISTS_ANSWER", new JSONPacket("CHAT", new Dictionary<String, Object>()
            {
                { "COMMAND", "ROOM_EXISTS" },
                { "ROOM_UID", a_UID }
            }));

            if (t_Answer.Data["STATE"].Equals("FAILED"))
                return false;

            return t_Answer.Data["EXISTS"].Equals(1);
        }

        public static async Task<ChatRoom> GetRoom(String a_UID)
        {
            JSONPacket t_Answer = await NetworkListener.WaitForChatPacket("GET_ROOM_ANSWER", new JSONPacket("CHAT", new Dictionary<string, object>()
            {
                { "COMMAND", "GET_ROOM" },
                { "ROOM_UID", a_UID }
            }));

            if (t_Answer.Data["STATE"].Equals("FAILED"))
                return null;

            return SerializeRoom((Dictionary<String, Object>)t_Answer.Data["ROOM"]);
        }

        public static async Task<List<ChatRoom>> GetRoomsByName(String a_Name)
        {
            JSONPacket t_Answer = await NetworkListener.WaitForChatPacket("GET_ROOMS_BY_NAME_ANSWER", new JSONPacket("CHAT", new Dictionary<String, Object>()
            {
                { "COMMAND", "GET_ROOMS_BY_NAME" },
                { "ROOM_NAME", a_Name }
            }));

            if (t_Answer.Data["STATE"].Equals("FAILED"))
                return null;

            return ((ArrayList)t_Answer.Data["ROOMS"]).Cast<Dictionary<String, Object>>().Select(x => SerializeRoom(x)).ToList();
        }

        public static async Task<List<ChatRoom>> GetRoomsForUser(Int64 a_ID)
        {
            JSONPacket t_Answer = await NetworkListener.WaitForChatPacket("GET_ROOMS_FOR_USER_ANSWER", new JSONPacket("CHAT", new Dictionary<String, Object>()
            {
                { "COMMAND", "GET_ROOMS_FOR_USER" },
                { "USER_DBID", a_ID }
            }));

            if (t_Answer.Data["STATE"].Equals("FAILED"))
                return null;

            return ((ArrayList)t_Answer.Data["ROOMS"]).Cast<Dictionary<String, Object>>().Select(x => SerializeRoom(x)).ToList();
        }

        public static void UpdateRoom(ChatRoom a_Room)
        {
            NetworkListener.Client.Send(new JSONPacket("CHAT", new Dictionary<String, Object>()
            {
                { "COMMAND", "UPDATE_ROOM" },
                { "ROOM_INFO", SerializeRoom(a_Room) }
            }));
        }

        public static void SendMessage(String a_Message, String a_Target)
        {
            NetworkListener.Client.Send(new JSONPacket("CHAT", new Dictionary<string, object>()
            {
                { "COMMAND", "SEND_MESSAGE" },
                { "TARGET", a_Target },
                { "MESSAGE", a_Message.ToBase64() }
            }));

            MainWindow.m_Window.Dispatcher.BeginInvoke(new ThreadStart(async () => MainWindow.m_Window.m_MainPage.RenderChat(await Chat.GetRoom(NetworkListener.m_Lobby.UID))));
        }

        public static ChatRoom CopyRoom(ChatRoom a_Room, Boolean a_IgnorePasswordAndPermissions = true)
        {
            ChatRoom t_Return = new ChatRoom();
            t_Return.DefaultPermissions = a_Room.DefaultPermissions;
            t_Return.IsPrivate = a_Room.IsPrivate;
            t_Return.Name = a_Room.Name;
            t_Return.UID = a_Room.UID;

            if (!a_IgnorePasswordAndPermissions)
                t_Return.Password = a_Room.Password;

            foreach (ChatEntry t_SrcEntry in a_Room.m_History)
            {
                ChatEntry t_DstEntry = new ChatEntry();
                t_DstEntry.MessageStack.AddRange(t_SrcEntry.MessageStack);
                t_DstEntry.Sender = t_SrcEntry.Sender;
                t_DstEntry.Timestamp = t_SrcEntry.Timestamp;
                t_Return.m_History.Add(t_DstEntry);
            }

            foreach (ChatMember t_SrcMember in a_Room.m_Members)
            {
                ChatMember t_DstMember = new ChatMember();
                t_DstMember.DatabaseID = t_SrcMember.DatabaseID;
                t_DstMember.JoinTime = t_SrcMember.JoinTime;

                if (!a_IgnorePasswordAndPermissions)
                    t_DstMember.Permissions = t_SrcMember.Permissions;
                else
                    t_DstMember.Permissions = t_Return.DefaultPermissions;
            }

            return t_Return;
        }

        public static ChatRoom SerializeRoom(Dictionary<String, Object> a_Data)
        {
            ChatRoom t_Room = new ChatRoom();

            if (a_Data.ContainsKey("UID"))
                t_Room.UID = a_Data["UID"] as String;

            if (a_Data.ContainsKey("PASSWORD"))
                t_Room.Password = a_Data["PASSWORD"] as String;

            if (a_Data.ContainsKey("NAME"))
                t_Room.Name = a_Data["NAME"].ToString();

            if (a_Data.ContainsKey("IS_PRIVATE"))
                t_Room.IsPrivate = a_Data["IS_PRIVATE"].Equals(1);

            if (a_Data.ContainsKey("DEFAULT_PERMISSIONS"))
                t_Room.DefaultPermissions = (ChatPermission)Convert.ToInt32(a_Data["DEFAULT_PERMISSIONS"]);

            if (a_Data.ContainsKey("HISTORY"))
                t_Room.m_History = ((ArrayList)a_Data["HISTORY"]).Cast<Dictionary<String, Object>>().Select(x => SerializeChatEntry(x)).ToList();
            else
                t_Room.m_History = new List<ChatEntry>();

            if (a_Data.ContainsKey("MEMBERS"))
                t_Room.m_Members = ((ArrayList)a_Data["MEMBERS"]).Cast<Dictionary<String, Object>>().Select(x => SerializeChatMember(x)).ToList();
            else
                t_Room.m_Members = new List<ChatMember>();

            return t_Room;
        }

        public static ChatEntry SerializeChatEntry(Dictionary<String, Object> a_Data)
        {
            ChatEntry t_Return = new ChatEntry();
            t_Return.Sender = Convert.ToInt64(a_Data["SENDER"]);
            t_Return.Timestamp = new DateTime(Convert.ToInt64(a_Data["TIMESTAMP"]));
            t_Return.MessageStack = ((ArrayList)a_Data["MESSAGE_STACK"]).Cast<String>().ToList();
            return t_Return;
        }

        public static ChatMember SerializeChatMember(Dictionary<String, Object> a_Data)
        {
            ChatMember t_Return = new ChatMember();
            t_Return.DatabaseID = Convert.ToInt64(a_Data["DBID"]);
            t_Return.JoinTime = new DateTime(Convert.ToInt64(a_Data["JOIN_TIME"]));
            t_Return.Permissions = (ChatPermission)Convert.ToInt32(a_Data["PERMISSIONS"]);
            return t_Return;
        }

        public static Dictionary<String, Object> SerializeRoom(ChatRoom a_Room)
        {
            Dictionary<String, Object> t_Return = new Dictionary<String, Object>();
            t_Return.Add("UID", a_Room.UID);
            t_Return.Add("PASSWORD", a_Room.Password);
            t_Return.Add("NAME", a_Room.Name);
            t_Return.Add("IS_PRIVATE", Convert.ToInt32(a_Room.IsPrivate));
            t_Return.Add("DEFAULT_PERMISSIONS", a_Room.DefaultPermissions);
            t_Return.Add("HISTORY", a_Room.m_History.Select(x => SerializeChatEntry(x)).ToArray());
            t_Return.Add("MEMBERS", a_Room.m_Members.Select(x => SerializeChatMember(x)).ToArray());
            return t_Return;
        }

        public static Dictionary<String, Object> SerializeChatEntry(ChatEntry a_Entry)
        {
            Dictionary<String, Object> t_Return = new Dictionary<String, Object>();
            t_Return.Add("SENDER", a_Entry.Sender);
            t_Return.Add("TIMESTAMP", a_Entry.Timestamp);
            t_Return.Add("MESSAGE_STACK", a_Entry.MessageStack.ToArray());
            return t_Return;
        }

        public static Dictionary<String, Object> SerializeChatMember(ChatMember a_Member)
        {
            Dictionary<String, Object> t_Return = new Dictionary<String, Object>();
            t_Return.Add("DBID", a_Member.DatabaseID);
            t_Return.Add("JOIN_TIME", a_Member.JoinTime);
            t_Return.Add("PERMISSIONS", a_Member.Permissions);
            return t_Return;
        }

        public static void LeaveRoom(String a_RoomUID)
        {
            NetworkListener.Client.Send(new JSONPacket("CHAT", new Dictionary<String, Object>()
            {
                { "COMMAND", "LEAVE_ROOM" },
                { "TARGET", a_RoomUID }
            }));
        }
    }

    public class ChatRoom
    {
        public String Name { get; set; }
        public String Password { get; set; }
        public Boolean IsPrivate { get; set; }
        public List<ChatMember> m_Members = new List<ChatMember>();
        public List<ChatEntry> m_History = new List<ChatEntry>();
        public String UID { get; set; }
        public ChatPermission DefaultPermissions { get; set; }

        public ChatRoom()
        {
            Name = "Unnamed room";
            IsPrivate = false;
            Password = null;
            UID = "";
        }

        public Boolean HasUser(Int64 a_DBID)
        {
            foreach (ChatMember t_Member in m_Members)
                if (t_Member.DatabaseID == a_DBID)
                    return true;

            return false;
        }
    }

    public class ChatEntry
    {
        public Int64 Sender { get; set; }
        public DateTime Timestamp { get; set; }
        public List<String> MessageStack = new List<String>();
    }

    public class ChatMember
    {
        public Int64 DatabaseID { get; set; }
        public DateTime JoinTime { get; set; }
        public ChatPermission Permissions { get; set; }
    }

    [Flags]
    public enum ChatPermission
    {
        READ = 1,
        WRITE = 2,
        INVITE = 4,
        KICK = 8,
        BAN = 16,
        DELETE_MESSAGE_OWN = 32,
        DELETE_MESSAGE_OTHERS = 64,
        DELETE_ROOM = 128
    }
}
