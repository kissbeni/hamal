﻿using System;
using System.Collections.Generic;

namespace SkypeLikeStuff
{
    public class UUID
    {
        private static long uniqueue_helper = 0;
        public byte Seed { private set; get; }
        public DateTime CreationTime { private set; get; }
        public bool IsParsed { private set; get; }
        public short PartHelper { private set; get; }
        public short UUID_Part0 { private set; get; }
        public short UUID_Part1 { private set; get; }
        public long UUID_Part2 { private set; get; }
        public int UUID_Part3 { private set; get; }
        public byte[] UUID_Part0_Bytes { private set; get; }
        public byte[] UUID_Part1_Bytes { private set; get; }
        public byte[] UUID_Part2_Bytes { private set; get; }
        public byte[] UUID_Part3_Bytes { private set; get; }

        public static implicit operator string(UUID uid)
        {
            if (uid == null)
                return null;

            return uid.ToString();
        }

        public UUID()
        {
            Seed = 0;
            CreationTime = DateTime.Now;
            IsParsed = false;
            CreateUUID();
        }

        public UUID(byte seed)
        {
            Seed = seed;
            CreationTime = DateTime.Now;
            IsParsed = false;
            CreateUUID();
        }

        public UUID(string uuid)
        {
            if (uuid.Split('-').Length != 4)
                goto invalidUUID;

            string[] parts = uuid.Split('-');

            short p0 = 0;
            short p1 = 0;
            long p2 = 0;
            int p3 = 0;

            for (int i = 0; i < parts.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        if (!Int16.TryParse(parts[i], out p0))
                            goto invalidUUID;

                        break;

                    case 1:
                        if (!Int16.TryParse(parts[i], out p1))
                            goto invalidUUID;

                        break;

                    case 2:
                        if (!Int64.TryParse(parts[i], out p2))
                            goto invalidUUID;

                        break;

                    case 3:
                        if (!Int32.TryParse(parts[i], out p3))
                            goto invalidUUID;

                        break;
                }
            }

            UUID_Part0 = p0;
            UUID_Part1 = p1;
            UUID_Part2 = p2;
            UUID_Part3 = p3;
            IsParsed = true;

            return;
            invalidUUID:
            throw new ArgumentException("Invalid uuid", "uuid");
        }

        public UUID(byte[] uuid)
        {
            UUID_Part0 = BitConverter.ToInt16(uuid, 0);
            UUID_Part1 = BitConverter.ToInt16(uuid, 2);
            UUID_Part2 = BitConverter.ToInt64(uuid, 4);
            UUID_Part3 = BitConverter.ToInt32(uuid, 12);

            UUID_Part0_Bytes = BitConverter.GetBytes(UUID_Part0);
            UUID_Part1_Bytes = BitConverter.GetBytes(UUID_Part1);
            UUID_Part2_Bytes = BitConverter.GetBytes(UUID_Part2);
            UUID_Part3_Bytes = BitConverter.GetBytes(UUID_Part3);
            IsParsed = true;
        }

        private void CreateUUID()
        {
            if (!IsParsed)
            {
                PartHelper = (short)(DateTime.Now.Millisecond | DateTime.Now.Second);
                UUID_Part0 = (short)(DateTime.Now.Year & PartHelper);
                UUID_Part1 = (short)(DateTime.Now.Month | DateTime.Now.Day & PartHelper);
                UUID_Part2 = DateTime.Now.Ticks | (uint)Environment.TickCount;
                UUID_Part3 = DateTime.Now.Hour | DateTime.Now.Minute ^ DateTime.Now.Second & PartHelper;

                UUID_Part2 += uniqueue_helper++;
            }

            UUID_Part0_Bytes = BitConverter.GetBytes(UUID_Part0);
            UUID_Part1_Bytes = BitConverter.GetBytes(UUID_Part1);
            UUID_Part2_Bytes = BitConverter.GetBytes(UUID_Part2);
            UUID_Part3_Bytes = BitConverter.GetBytes(UUID_Part3);

            if (Seed != 0)
            {
                if (Seed % 2 == 0)
                    for (int i = 0; i < UUID_Part0_Bytes.Length; i++)
                        UUID_Part0_Bytes[i] = (byte)(UUID_Part0_Bytes[i] ^ Seed);

                if (Seed % 3 == 0)
                    for (int i = 0; i < UUID_Part1_Bytes.Length; i++)
                        UUID_Part1_Bytes[i] = (byte)(UUID_Part1_Bytes[i] ^ Seed);

                if (Seed % 6 == 0)
                    for (int i = 0; i < UUID_Part2_Bytes.Length; i++)
                        UUID_Part2_Bytes[i] = (byte)(UUID_Part2_Bytes[i] ^ Seed);

                if (Seed % 12 == 0)
                    for (int i = 0; i < UUID_Part3_Bytes.Length; i++)
                        UUID_Part3_Bytes[i] = (byte)(UUID_Part3_Bytes[i] ^ Seed);
            }
        }

        public byte[] ToBytes()
        {
            List<byte> bytes = new List<byte>();
            bytes.AddRange(UUID_Part0_Bytes);
            bytes.AddRange(UUID_Part1_Bytes);
            bytes.AddRange(UUID_Part2_Bytes);
            bytes.AddRange(UUID_Part3_Bytes);
            return bytes.ToArray();
        }

        public override string ToString()
        {
            return String.Format("{0}-{1}-{2}-{3}",
                BitConverter.ToString(UUID_Part0_Bytes).Replace("-", "").ToLower(),
                BitConverter.ToString(UUID_Part1_Bytes).Replace("-", "").ToLower(),
                BitConverter.ToString(UUID_Part2_Bytes).Replace("-", "").ToLower(),
                BitConverter.ToString(UUID_Part3_Bytes).Replace("-", "").ToLower());
        }
    }
}
