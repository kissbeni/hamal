﻿#define RichTextBoxForChat

using JSocket;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace Hamal.Client
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        private UserStatus m_LastStatus = UserStatus.ONLINE;
        public System.Timers.Timer m_Timer = new System.Timers.Timer();
        private List<ChatRoom> m_ChatRooms = new List<ChatRoom>();
        public ChatRoom m_CurrentRoom = null;

        public MainPage()
        {
            InitializeComponent();
            
            ((ComboBoxItem)StatusCB.Items[0]).Tag = UserStatus.ONLINE;
            ((ComboBoxItem)StatusCB.Items[1]).Tag = UserStatus.BUSY;
            ((ComboBoxItem)StatusCB.Items[2]).Tag = UserStatus.AWAY;
            ((ComboBoxItem)StatusCB.Items[3]).Tag = UserStatus.HIDDEN;
            ((ComboBoxItem)StatusCB.Items[4]).Tag = UserStatus.OFFLINE;

            m_Timer.Interval = 1000;
            TimeSpan t_TimeSpan = new TimeSpan();
            m_Timer.Elapsed += (la_Sender, la_Args) => Dispatcher.BeginInvoke(new ThreadStart(() =>
            {
                t_TimeSpan += TimeSpan.FromSeconds(1);

                String t_Text = "";

                if (t_TimeSpan.Days > 0)
                    t_Text += t_TimeSpan.Days + "d ";

                if (t_TimeSpan.Hours > 0)
                    t_Text += t_TimeSpan.Hours + "h ";

                if (t_TimeSpan.Minutes > 0)
                    t_Text += t_TimeSpan.Minutes + "m ";

                if (t_TimeSpan.Seconds >= 0)
                    t_Text += t_TimeSpan.Seconds + "s";

                TimeLabel.Content = t_Text;
            }));
        }

        public void Update()
        {
            Dispatcher.BeginInvoke(new ThreadStart(() =>
            {
                DisplayName.Content = NetworkListener.LoginInfo.DisplayName;
                StatusMsg.Text = NetworkListener.LoginInfo.StatusMessage;

                ProfileImage.Source = Helper.SafeLoadImage(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "ProfilePictures", NetworkListener.LoginInfo.DatabaseID + "-" + Helper.UniqueID + ".png"));

                ComboBoxItem t_Select = (ComboBoxItem)StatusCB.Items[0];
                foreach (ComboBoxItem t_Item in StatusCB.Items)
                    if ((UserStatus)t_Item.Tag == NetworkListener.LoginInfo.Status)
                    {
                        t_Select = t_Item;
                        m_LastStatus = NetworkListener.LoginInfo.Status;
                        break;
                    }

                StatusCB.SelectedItem = t_Select;
            }));

            Helper.RunTaskWithCallback<List<ChatRoom>>(Chat.GetRoomsForUser(NetworkListener.LoginInfo.DatabaseID), new Action<List<ChatRoom>>((la_List) =>
            {
                m_ChatRooms = la_List;

                /*Dispatcher.Invoke(CGCB.Items.Clear);

                foreach (ChatRoom t_Room in m_ChatRooms)
                {
                    Dispatcher.BeginInvoke(new ThreadStart(() =>
                    {
                        CGCB.Items.Add(t_Room.Name);

                        if ((m_CurrentRoom != null && t_Room.UID == m_CurrentRoom.UID) || (m_CurrentRoom == null && t_Room.Name == "Lobby"))
                            CGCB.SelectedIndex = CGCB.Items.Count - 1;
                    }));
                }*/
            }));
        }

        public void UpdateContacts()
        {
            Dispatcher.BeginInvoke(new ThreadStart(() =>
            {
                Users.Children.Clear();

                foreach (AuthenticationInfo t_Info in NetworkListener.m_Contacts.Values)
                {
                    UserItem t_UserItem = new UserItem();
                    Color t_StatusColor = Colors.Orange;

                    switch (t_Info.Status)
                    {
                        case UserStatus.AWAY:
                            t_StatusColor = Color.FromArgb(0xFF, 0xE6, 0xB8, 0x00);
                            break;

                        case UserStatus.BUSY:
                            t_StatusColor = Color.FromArgb(0xFF, 0xB2, 0x00, 0x00);
                            break;

                        case UserStatus.HIDDEN:
                            t_StatusColor = Colors.Pink;
                            break;

                        case UserStatus.OFFLINE:
                            t_StatusColor = Colors.Gray;
                            break;

                        case UserStatus.ONLINE:
                            t_StatusColor = Color.FromArgb(0xFF, 0x99, 0xCC, 0x00);
                            break;
                    }

                    t_UserItem.StatusIndicator.Fill = new SolidColorBrush(t_StatusColor);
                    t_UserItem.DisplayName.Content = t_Info.DisplayName;
                    t_UserItem.StatusMessage.Content = t_Info.StatusMessage;
                    t_UserItem.ProfileImage.Source = Helper.SafeLoadImage(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "ProfilePictures", t_Info.DatabaseID + "-" + Helper.UniqueID + ".png"));

                    Users.Children.Add(t_UserItem);
                }
            }));
        }

        private void StatusCB_SelectionChanged(Object a_Sender, SelectionChangedEventArgs a_Args)
        {
            UserStatus t_Status = (UserStatus)((ComboBoxItem)StatusCB.SelectedItem).Tag;

            if (m_LastStatus != t_Status)
            {
                m_LastStatus = t_Status;

                NetworkListener.Client.Send(new JSONPacket("SET_USER_STATUS", new Dictionary<string, object>() { { "NEW_STATE", t_Status } }));
            }
        }

        private void StatusMsg_KeyDown(Object a_Sender, KeyEventArgs a_Args)
        {
            if (a_Args.Key == Key.Enter && a_Args.IsRepeat == false && Keyboard.Modifiers == ModifierKeys.Control)
            {
                NetworkListener.Client.Send(new JSONPacket("SET_STATUS_MESSAGE", new Dictionary<string, object>()
                {
                    { "NEW_STATE", StatusMsg.Text.ToBase64() }
                }));

                a_Args.Handled = true;
            }
        }

        private void Button_Click(Object a_Sender, RoutedEventArgs a_Args)
        {
            String t_Msg = ChatInput.Text;

            if (t_Msg == null || t_Msg.Length < 1)
                return;

            Chat.SendMessage(t_Msg, m_CurrentRoom.UID);

            ChatInput.Text = "";
        }

        public void RenderChat(ChatRoom a_Room)
        {
            if (a_Room == null)
                return;

            if (m_CurrentRoom != null && m_CurrentRoom.UID != a_Room.UID)
            {
                Chat.LeaveRoom(a_Room.UID);
                Update();
            }

            if (m_CurrentRoom == null|| m_CurrentRoom.UID == a_Room.UID)
                m_CurrentRoom = a_Room;

            Dispatcher.Invoke(() =>
            {
                #if !RichTextBoxForChat
                ChatLog.Children.Clear();
                #else
                ChatLog.Document.Blocks.Clear();
                #endif

                RoomUserCount.Content = a_Room.m_Members.Count + " member(s)";
                RoomName.Content = a_Room.Name;
            
                foreach (ChatEntry t_Entry in a_Room.m_History)
                {
                    #if !RichTextBoxForChat
                    UIChatEntry uie = new UIChatEntry();

                    if (e.Sender == NetworkListener.LoginInfo.DatabaseID)
                        uie.Header.Content = NetworkListener.LoginInfo.DisplayName;
                    else if (NetworkListener.Contacts.ContainsKey(e.Sender))
                        uie.Header.Content = NetworkListener.Contacts[e.Sender].DisplayName;
                    else
                        uie.Header.Content = ">> >> No username found :( << <<";

                    uie.Header.DateLabel.Content = Helper.BeautyDate(e.Timestamp);

                    foreach (String s in e.MessageStack)
                        uie.Messages.Children.Add(new TextBlock() { Text = s.FromBase64() });

                    ChatLog.Children.Add(uie);
                    #else
                    foreach (String t_Msg in t_Entry.MessageStack)
                    {
                        TextRange t_Range = new TextRange(ChatLog.Document.ContentEnd, ChatLog.Document.ContentEnd);

                        if (t_Entry.Sender == NetworkListener.LoginInfo.DatabaseID)
                            t_Range.Text = "[" + Helper.BeautyDate(t_Entry.Timestamp) + "] " + NetworkListener.LoginInfo.DisplayName + ": ";
                        else if (NetworkListener.m_Contacts.ContainsKey(t_Entry.Sender))
                            t_Range.Text = "[" + Helper.BeautyDate(t_Entry.Timestamp) + "] " + NetworkListener.m_Contacts[t_Entry.Sender].DisplayName + ": ";
                        else
                            t_Range.Text = "[" + Helper.BeautyDate(t_Entry.Timestamp) + "] " + "UNKNOWN: ";

                        t_Range.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.Yellow);
                        t_Range = new TextRange(ChatLog.Document.ContentEnd, ChatLog.Document.ContentEnd);
                        t_Range.Text = t_Msg.FromBase64() + Environment.NewLine;
                        t_Range.ApplyPropertyValue(TextElement.ForegroundProperty, Brushes.White);
                        ChatLog.ScrollToEnd();
                    }
                    #endif
                }
            });
        }

        private void CreateRoomButton_Click(Object a_Sender, RoutedEventArgs a_Args)
        {
            Helper.RunTaskWithCallback<ChatRoom>(Chat.CreateOrJoinRoom(null, null, null, true), new Action<ChatRoom>((la_Room) => RenderChat(la_Room)));
        }

        private void ChatInput_KeyUp(Object a_Sender, KeyEventArgs a_Args)
        {
            if (a_Args.Key == Key.Enter)
                Button_Click(a_Sender, null);
        }
    }
}
