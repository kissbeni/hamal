﻿using System;
using System.ComponentModel;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace Hamal.Client
{
    /// <summary>
    /// Interaction logic for LoginPanel.xaml
    /// </summary>
    public partial class LoginPanel : Page
    {
        public LoginPanel()
        {
            InitializeComponent();
        }

        public void ToggleLoginView(Boolean a_LoggingIn)
        {
            LoginGrid.Visibility = a_LoggingIn ? Visibility.Hidden : Visibility.Visible;
            LoggingInGrid.Visibility = a_LoggingIn ? Visibility.Visible : Visibility.Hidden;
        }

        private void LoginButton_Click(Object a_Sender, RoutedEventArgs a_Args)
        {
            if (UsernameBox.Text.Length < 1 || PasswordBox.Password.Length < 1)
                return;

            ToggleLoginView(true);

            BackgroundWorker t_Worker = new BackgroundWorker();

            String t_Username = UsernameBox.Text;
            String t_Password = PasswordBox.Password;

            t_Worker.DoWork += (la_Sender, la_Args) =>
            {
                NetworkListener.Init(IPAddress.Parse("127.0.0.1"), 32, t_Username, t_Password);
                while (!NetworkListener.GotLoginAnswer) ;
            };

            t_Worker.RunWorkerCompleted += (la_Sender, la_Args) =>
            {
                if (NetworkListener.LoginInfo.State != AuthenticationState.LOGIN_OK)
                {
                    switch (NetworkListener.LoginInfo.State)
                    {
                        case AuthenticationState.LOGIN_USERNAME_BAD:
                            LoginErrorLabel.Content = "Invalid username";
                            break;
                        case AuthenticationState.LOGIN_PASSWORD_BAD:
                            LoginErrorLabel.Content = "Invalid password";
                            break;
                        case AuthenticationState.LOGIN_SERVICE_BUSY:
                            LoginErrorLabel.Content = "Login service is busy";
                            break;
                    };

                    ToggleLoginView(false);
                }
            };

            t_Worker.RunWorkerAsync();
        }

        private void Page_Loaded(Object a_Sender, RoutedEventArgs a_Args)
        {
            if (Properties.Settings.Default.Username != null && Properties.Settings.Default.Username.Length > 0)
            {
                UsernameBox.Text = Properties.Settings.Default.Username;
                SaveUsernameCheckbox.IsChecked = true;
            }

            if (Properties.Settings.Default.Password != null && Properties.Settings.Default.Password.Length > 0)
            {
                PasswordBox.Password = Properties.Settings.Default.Password;
                SavePasswordCheckbox.IsChecked = true;
            }
        }

        internal void LoginError(AuthenticationState a_State)
        {
            Dispatcher.BeginInvoke(new ThreadStart(() =>
            {
                ToggleLoginView(false);
                LoginErrorLabel.Content = a_State.ToString();
            }));
        }
    }
}
