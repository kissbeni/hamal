﻿using JSocket;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Hamal.Client
{
    [Flags]
    public enum AuthenticationState
    {
        LOGIN_OK = 0,
        LOGIN_USERNAME_BAD = 1,
        LOGIN_PASSWORD_BAD = 2,
        LOGIN_SERVICE_BUSY = 4
    }

    public enum UserStatus
    {
        ONLINE,
        BUSY,
        AWAY,
        HIDDEN,
        OFFLINE
    }

    public class AuthenticationInfo
    {
        public AuthenticationState State { get; set; }
        public String DisplayName { get; set; }
        public String UUID { get; set; }
        public UserStatus Status { get; set; }
        public String StatusMessage { get; set; }
        public Int64 DatabaseID { get; set; }
    }

    public class NetworkListener
    {
        public static DateTime ServerTime { get; private set; }
        public static JSONClient Client { get; private set; }
        public static String Username { get; private set; }
        public static String Password { get; private set; }
        public static Boolean GotLoginAnswer { get; private set; }
        public static AuthenticationInfo LoginInfo { get; private set; }
        public static Dictionary<Int64, AuthenticationInfo> m_Contacts = new Dictionary<Int64, AuthenticationInfo>();

        public static ChatRoom m_Lobby = null;

        public static void Init(IPAddress a_Address, UInt16 a_Port, String a_Username, String a_Password)
        {
            Username = a_Username;
            Password = a_Password;
            GotLoginAnswer = false;

            Kill();

            m_Contacts.Clear();
            Client = new JSONClient(a_Address, a_Port);
            Client.OnMessageReceive += MessageRecieved;
            Client.OnPacketReceive += PacketRecieved;
            Client.OnKeepAlive += OnKeepAlive;
            Client.Start(true);
        }

        public static void Kill()
        {
            if (Client != null && Client.Online)
                Client.Stop();
        }

        private static List<JSONPacket> m_ChatPackets = new List<JSONPacket>();
        private static List<JSONPacket> m_PlainPackets = new List<JSONPacket>();
        public const Int32 PacketResendTimeout = 5000;
        public static Boolean m_AutoUpdate = false;

        public static async Task<JSONPacket> WaitForChatPacket(String a_Command, JSONPacket a_Packet)
        {
            g_Begin:

            Client.Send(a_Packet);
            Int32 t_Timer = 0;

            while (m_ChatPackets.Count == 0 || m_ChatPackets.Where(x => x.Data.ContainsKey("COMMAND") && x.Data["COMMAND"].Equals(a_Command)).Count() == 0)
            {
                if (t_Timer >= PacketResendTimeout)
                    goto g_Begin;

                String t_Data = "";
                foreach (JSONPacket t_Packet in m_ChatPackets)
                    t_Data += " " + t_Packet.Data["COMMAND"].ToString();

                t_Timer += 100;
                await Task.Delay(100);
            }

            JSONPacket t_FoundPacket = m_ChatPackets.Where(x => x.Data.ContainsKey("COMMAND") && x.Data["COMMAND"].Equals(a_Command)).First();

            if (m_ChatPackets.Contains(t_FoundPacket))
                try
                {
                    m_ChatPackets.Remove(t_FoundPacket);
                }
                catch
                {
                    System.Diagnostics.Debug.WriteLine("Dead chat packet: " + t_FoundPacket.Data["COMMAND"]);
                }
            else
                System.Diagnostics.Debug.WriteLine("Dead chat packet: " + t_FoundPacket.Data["COMMAND"]);

            return t_FoundPacket;
        }

        /*public static JSONPacket WaitForPacket(String answer_name, JSONPacket get)
        {
            send_begin:

            Client.Send(get);
            Int32 Timer = 0;

            while (PlainPackets.Count == 0 || PlainPackets.Where(x => x.PacketName.Equals(answer_name)).Count() == 0)
            {
                if (Timer >= PacketResendTimeout)
                    goto send_begin;

                Timer += 100;
                Thread.Sleep(100);
            }

            JSONPacket foundPacket = PlainPackets.Where(x => x.PacketName.Equals(answer_name)).First();
            PlainPackets.Remove(foundPacket);

            return foundPacket;
        }*/

        public static void OnKeepAlive(JSONClient a_Client)
        {
            if (!m_AutoUpdate)
                return;

            MainWindow.m_Window.Dispatcher.BeginInvoke(new ThreadStart(() =>
            {
                MainWindow.m_Window.m_MainPage.UpdateContacts();

                Action<ChatRoom> t_RenderChatCallback = new Action<ChatRoom>((la_Room) => MainWindow.m_Window.m_MainPage.RenderChat(la_Room));

                if (MainWindow.m_Window.m_MainPage.m_CurrentRoom == null)
                    Helper.RunTaskWithCallback(Chat.GetRoom(NetworkListener.m_Lobby.UID), t_RenderChatCallback);
                else
                    Helper.RunTaskWithCallback(Chat.GetRoom(MainWindow.m_Window.m_MainPage.m_CurrentRoom.UID), t_RenderChatCallback);
            }));

            MainWindow.m_Window.m_MainPage.Update();
        }

        public static async void PacketRecieved(JSONClient a_Client, JSONPacket a_Packet)
        {
            Client = a_Client;

            if (a_Packet == null)
                return;

            if (a_Packet.PacketName == "CHAT")
            {
                if (!a_Packet.Data.ContainsKey("COMMAND"))
                    return;

                System.Diagnostics.Debug.WriteLine("Chat packet: " + a_Packet.Data["COMMAND"]);

                if (a_Packet.Data["COMMAND"].Equals("GOT_MESSAGE"))
                {
                    // TODO: Make async
                    ChatRoom t_Room = await Chat.GetRoom(a_Packet.Data["UID"].ToString());
                    t_Room.m_History = ((ArrayList)a_Packet.Data["HISTORY"]).ToArray().Select(x => Chat.SerializeChatEntry((Dictionary<String, Object>)x)).ToList();
                    Chat.UpdateRoom(t_Room/*, true*/);
                }
                else
                    m_ChatPackets.Add(a_Packet);

                return;
            }

            if (a_Packet.PacketName == "USER_DATA_ANSWER")
            {
                AuthenticationInfo t_Info = new AuthenticationInfo();
                t_Info.DatabaseID = Convert.ToInt64(a_Packet.Data["USER_DBID"]);

                Dictionary<String, Object> t_UserInfo = (Dictionary<String, Object>)a_Packet.Data["USER_INFO"];
                t_Info.DisplayName = t_UserInfo["DISPLAY_NAME"].ToString().FromBase64();
                t_Info.UUID = t_UserInfo["UUID"].ToString();
                t_Info.Status = (UserStatus)Enum.Parse(typeof(UserStatus), t_UserInfo["STATUS"].ToString(), true);
                
                if (t_UserInfo["STATUS_MESSAGE"] != null)
                    t_Info.StatusMessage = t_UserInfo["STATUS_MESSAGE"].ToString().FromBase64();

                a_Client.Send(new JSONPacket("GET_ICON", new Dictionary<String, Object>() { { "DBID", t_Info.DatabaseID } }));

                if (!m_Contacts.ContainsKey(t_Info.DatabaseID))
                    m_Contacts.Add(t_Info.DatabaseID, t_Info);
                else
                    m_Contacts[t_Info.DatabaseID] = t_Info;

                return;
            }

            if (a_Packet.PacketName == "LOGIN_ANSWER")
            {
                AuthenticationInfo t_Info = new AuthenticationInfo();
                LoginInfo = t_Info;

                t_Info.State = (AuthenticationState)(Int32)a_Packet.Data["STATE"];

                if (t_Info.State == AuthenticationState.LOGIN_OK)
                {
                    t_Info.DisplayName = a_Packet.Data["DISPLAY_NAME"].ToString().FromBase64();
                    t_Info.DatabaseID = (Int32)a_Packet.Data["DATABASE_ID"];
                    t_Info.UUID = a_Packet.Data["UUID"].ToString();
                    t_Info.Status = (UserStatus)(int)a_Packet.Data["STATUS"];
                    t_Info.StatusMessage = a_Packet.Data["STATUS_MESSAGE"].ToString().FromBase64();
                    a_Client.Send(new JSONPacket("GET_ICON", new Dictionary<String, Object>() { { "DBID", t_Info.DatabaseID } }));

                    await MainWindow.m_Window.Dispatcher.BeginInvoke(new ThreadStart(async () =>
                    {
                        List<ChatRoom> t_Rooms = await Chat.GetRoomsByName("Lobby");

                        if (t_Rooms != null && t_Rooms.Count > 0)
                        {
                            t_Rooms = t_Rooms.Where(x => !x.IsPrivate && String.IsNullOrEmpty(x.Password)).ToList();

                            if (t_Rooms != null && t_Rooms.Count > 0)
                            {
                                ChatRoom t_Room = t_Rooms.First();

                                if (t_Room != null)
                                {
                                    m_Lobby = await Chat.CreateOrJoinRoom(t_Room.UID, t_Room.Name, t_Room.Password, t_Room.IsPrivate);

                                    goto g_End;
                                }
                            }
                        }

                        m_Lobby = await Chat.CreateOrJoinRoom(null, "Lobby", null, false);

                    g_End:
                        a_Client.Send(new JSONPacket("GET_CONTACTS", new Dictionary<string, object>()));
                        Thread.Sleep(Convert.ToInt32(Client.Ping + 200));
                        a_Client.Send(new JSONPacket("REQUEST_PAGE", new Dictionary<string, object>() { { "PAGE", "MAIN_PAGE" } }));

                        m_AutoUpdate = true;

                        MainWindow.m_Window.m_MainPage.m_Timer.Start();
                    }));
                }
                else
                    MainWindow.m_Window.LoginError(t_Info.State);

                GotLoginAnswer = true;
                return;
            }

            if (a_Packet.PacketName == "SET_PAGE")
            {
                String t_Page = a_Packet.Data["PAGE"].ToString();

                switch (t_Page)
                {
                    case "LOGIN_PAGE":
                        MainWindow.m_Window.LoginPage();
                        break;
                    case "MAIN_PAGE":
                        MainWindow.m_Window.MainPage();
                        break;
                }

                return;
            }

            if (a_Packet.PacketName == "CONTACTS_ANSWER")
            {
                foreach (Object t_Object in ((ArrayList)a_Packet.Data["CONTACTS"]))
                {
                    Int64 t_DBID = Convert.ToInt64(t_Object);
                    a_Client.Send(new JSONPacket("GET_USER_BY_ID", new Dictionary<string, object>() { { "USER_DBID", t_DBID } }));
                }

                return;
            }

            if (a_Packet.PacketName == "SHOW_ALERT")
            {
                MessageBoxButton t_MsgBoxButton = MessageBoxButton.OK;
                MessageBoxImage t_MsgBoxIcon = MessageBoxImage.None;
                String t_Title = "";
                String t_Message = "";
                String t_UID = "";

                if (a_Packet.Data.ContainsKey("TITLE"))
                    t_Title = a_Packet.Data["TITLE"] as String;

                if (a_Packet.Data.ContainsKey("MESSAGE"))
                    t_Message = a_Packet.Data["MESSAGE"] as String;

                if (a_Packet.Data.ContainsKey("BUTTONS"))
                    t_MsgBoxButton = (MessageBoxButton)Convert.ToInt32(a_Packet.Data["BUTTONS"]);

                if (a_Packet.Data.ContainsKey("ICON"))
                    t_MsgBoxIcon = (MessageBoxImage)Convert.ToInt32(a_Packet.Data["ICON"]);

                if (a_Packet.Data.ContainsKey("UID"))
                    t_UID = a_Packet.Data["UID"] as String;
                else
                    return;

                MessageBoxResult t_Result = MessageBoxResult.None;
                MainWindow.m_Window.Dispatcher.BeginInvoke(new ThreadStart(() => t_Result = MessageBox.Show(MainWindow.m_Window, t_Message, t_Title, t_MsgBoxButton, t_MsgBoxIcon)));

                Client.Send(new JSONPacket("ALERT_RESULT", new Dictionary<String, Object>() { { "UID", t_UID }, { "RESULT", (Int32)t_Result } }));

                return;
            }

            if (a_Packet.PacketName == "ICON_INFO")
            {
                Byte[] t_Data = Convert.FromBase64String(a_Packet.Data["ICON_BASE64"] as String);
                Int64 t_DBID = Convert.ToInt64(a_Packet.Data["DBID"]);

                if (!Directory.Exists("ProfilePictures"))
                    Directory.CreateDirectory("ProfilePictures");

                try
                {
                    File.WriteAllBytes(Path.Combine(Directory.GetCurrentDirectory(), "ProfilePictures", t_DBID + "-" + Helper.UniqueID + ".png"), t_Data);
                }
                catch
                { }
            }
        }

        public static void MessageRecieved(JSONClient t_Client)
        {
            if (t_Client.LastMessage == "CONNECTION_OK")
            {
                t_Client.Send(new JSONPacket("LOGIN", new Dictionary<String, Object>()
                {
                    { "Username", Username.ToBase64() },
                    { "Password", Password.ToBase64() }
                }));

                return;
            }
        }
    }
}
