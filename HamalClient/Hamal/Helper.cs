﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Hamal.Client
{
    public static class Helper
    {
        public static Int64 UniqueID = DateTime.Now.Ticks;

        public static void RunTaskWithCallback<T>(Task<T> a_Task, Action<T> a_Callback)
        {
            Action t_Action = new Action(async () =>
            {
                T t_Res = await a_Task;
                a_Callback.BeginInvoke(t_Res, null, null);
            });

            t_Action.BeginInvoke(null, null);
        }

        public static void RunTaskWithCallback(Task a_Task, Action a_Callback)
        {
            Action t_Action = new Action(async () =>
            {
                await a_Task;
                a_Callback.BeginInvoke(null, null);
            });

            t_Action.BeginInvoke(null, null);
        }

        public static BitmapSource SafeLoadImage(String a_Path)
        {
            if (!File.Exists(a_Path))
                return new BitmapImage(new Uri("/Hamal.Client;component/unk.png", UriKind.RelativeOrAbsolute)); // load dummy image

            Byte[] t_Bytes = File.ReadAllBytes(a_Path);

            using (MemoryStream t_MemoryStream = new MemoryStream(t_Bytes))
            {
                t_MemoryStream.Position = 0;

                BitmapImage t_Result = new BitmapImage();
                t_Result.BeginInit();
                t_Result.CacheOption = BitmapCacheOption.OnLoad;
                t_Result.StreamSource = t_MemoryStream;
                t_Result.EndInit();
                t_Result.Freeze();
                return t_Result;
            }
        }

        public static String ToBase64(this String a_ThisStr)
        {
            if (a_ThisStr == null)
                return null;

            return Convert.ToBase64String(Encoding.Default.GetBytes(a_ThisStr));
        }

        public static String FromBase64(this String a_ThisStr)
        {
            if (a_ThisStr == null)
                return null;

            return Encoding.Default.GetString(Convert.FromBase64String(a_ThisStr));
        }

        public static String BeautyDate(DateTime a_DateTime)
        {
            DateTime t_Now = DateTime.Now;

            if (t_Now.Day - 1 == a_DateTime.Day)
                return "Yesterday at " + a_DateTime.ToShortTimeString();

            if (t_Now.Day == a_DateTime.Day)
            {
                if (t_Now.Hour == a_DateTime.Hour)
                {
                    if (t_Now.Minute == a_DateTime.Minute)
                        return "Just now";

                    TimeSpan t_Diff = t_Now - a_DateTime;

                    return t_Diff.Minutes.ToString() + (t_Diff.Minutes == 1 ? " minute ago" : " minutes ago");
                }

                return "Today at " + a_DateTime.ToShortTimeString();
            }

            if (t_Now.Day + 1 == a_DateTime.Day)
                return "Tomorrow at " + a_DateTime.ToShortTimeString();

            return String.Format("{0}.{1}.{2}. {3}", a_DateTime.Year, a_DateTime.Month, a_DateTime.Day, a_DateTime.ToShortTimeString());
        }
    }
}
