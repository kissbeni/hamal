﻿using System;
using System.IO;
using System.Text;

namespace JSocket
{
    public partial class JSONPacket
    {
        public Byte[] GetBytes()
        {
            Encoding t_Encoding = Encoding.UTF8;

            using (MemoryStream t_MemoryStream = new MemoryStream())
            {
                using (BinaryWriter t_BinaryWriter = new BinaryWriter(t_MemoryStream))
                {
                    t_BinaryWriter.Write(t_Encoding.GetBytes(PacketName));
                    t_BinaryWriter.Write((Byte)0);

                    Utils.WriteJSONDictionary(t_BinaryWriter, t_Encoding, Data);
                }

                File.WriteAllBytes("debug.d", t_MemoryStream.ToArray());

                return t_MemoryStream.ToArray();
            }
        }
    }
}
