﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace JSocket
{
    public class JSONServer
    {
        #region Events
        public delegate void ClientEventHandler(RemoteClient a_Client, JSONServer a_Server);
        public delegate void ClientPacketEventHandler(RemoteClient a_Client, JSONServer a_Server, JSONPacket a_Packet);
        public event ClientEventHandler OnClientConnect;
        public event ClientEventHandler OnClientDisconnect;
        public event ClientEventHandler OnMessageReceive;
        public event ClientEventHandler OnKeepAlive;
        public event ClientPacketEventHandler OnPacketReceive;

        private void DispatchClientConnect(RemoteClient a_Client)
        {
            if (OnClientConnect != null)
                OnClientConnect.BeginInvoke(a_Client, this, null, null);
        }

        private void DispatchClientDisconnect(RemoteClient a_Client)
        {
            if (OnClientDisconnect != null)
                OnClientDisconnect.BeginInvoke(a_Client, this, null, null);
        }

        private void DispatchMessageReceive(RemoteClient a_Client)
        {
            if (OnMessageReceive != null)
                OnMessageReceive.BeginInvoke(a_Client, this, null, null);
        }

        private void DispatchKeepAlive(RemoteClient a_Client)
        {
            if (OnKeepAlive != null)
                OnKeepAlive.BeginInvoke(a_Client, this, null, null);
        }

        private void DispatchPacketReceive(RemoteClient a_Client, JSONPacket a_Packet)
        {
            if (OnPacketReceive != null)
                OnPacketReceive.BeginInvoke(a_Client, this, a_Packet, null, null);
        }
        #endregion

        /// <summary>
        /// The server's port
        /// </summary>
        public UInt16 Port { get; private set; }

        /// <summary>
        /// Server status
        /// </summary>
        public Boolean Online { get; private set; }
        
        /// <summary>
        /// Allow debug messages
        /// </summary>
        public Boolean Debug { get; set; }
        
        /// <summary>
        /// The main socket for communication
        /// </summary>
        public TcpListener TcpServer { get; private set; }
        
        /// <summary>
        /// Timeout for clients (in miliseconds)
        /// </summary>
        public Int32 Timeout { get; set; }
        
        /// <summary>
        /// The peroid of keep alive packets (in miliseconds)
        /// </summary>
        public Int32 KeepAlivePeriod { get; private set; }

        private void DebugLine(String a_Format, params Object[] a_Args)
        {
            if (!Debug)
                return;

            Console.WriteLine(a_Format, a_Args);
        }

        /// <summary>
        /// Create a new server instance
        /// </summary>
        /// <param name="a_Port">The server's port</param>
        public JSONServer(UInt16 a_Port = 32, Int32 a_KeepAlivePeroid = 5000)
        {
            if (a_KeepAlivePeroid < 500)
                throw new Exception("The keep alive peroid must bigger than a half second!");

            Port = a_Port;
            KeepAlivePeriod = a_KeepAlivePeroid;
            DebugLine("Created! Port: {0}", a_Port);

            TcpServer = new TcpListener(IPAddress.Any, Port);
            TcpServer.Start();
        }

        /// <summary>
        /// Starts the listener loop
        /// </summary>
        /// <param name="a_Async">If true, the server will run in an other thread.</param>
        public void Start(Boolean a_Async = false)
        {
            if (Online)
            {
                DebugLine("You can't start the server, because its already running!");
                return;
            }

            DebugLine("Starting server...");

            Online = true;

            if (!a_Async)
                Listen();
            else
                new Thread(new ThreadStart(Listen)).Start();
        }

        /// <summary>
        /// Stops the listener loop
        /// </summary>
        public void Stop()
        {
            Online = false;
        }

        private void Listen()
        {
            DebugLine("Server loop start");

            while (Online)
            {
                TcpClient t_Client = TcpServer.AcceptTcpClient();

                Thread t_ClientThread = new Thread(new ParameterizedThreadStart(ClientListener));
                t_ClientThread.Start(t_Client);
            }

            DebugLine("Server loop end");

            Online = false;
        }

        private void ClientListener(Object a_Client)
        {
            TcpClient t_TcpClient = (TcpClient)a_Client;
            NetworkStream t_ClientStream = t_TcpClient.GetStream();
            RemoteClient t_RemoteClient = new RemoteClient();
            t_RemoteClient.Status = ClientStatus.CLIENT_STATE_NOW_CONNECTED;
            t_RemoteClient.TcpClient = t_TcpClient;
            t_RemoteClient.TcpStream = t_ClientStream;

            DispatchClientConnect(t_RemoteClient);

            DebugLine("Client connected");

            Byte[] t_Message = new Byte[4096];
            Int32 t_BytesRead;

            Int32 t_FirstKeepAlives = 0;

            Thread t_KeepAliveThread = new Thread(new ParameterizedThreadStart((la_RemoteClient) =>
            {
                while (true)
                {
                    if (((RemoteClient)la_RemoteClient).ReceivedKeepAlive)
                    {
                        DateTime t_Now = DateTime.Now;
                        Dictionary<String, Object> t_KeepAliveData = new Dictionary<String, Object>();
                        t_KeepAliveData.Add("Time", t_Now.Ticks);
                        
                        if (t_FirstKeepAlives < 4)
                        {
                            t_KeepAliveData.Add("ClientID", ((RemoteClient)la_RemoteClient).ClientID);
                            t_KeepAliveData.Add("KeepAlivePeroid", KeepAlivePeriod);
                        }

                        t_KeepAliveData.Add("Ping", ((RemoteClient)la_RemoteClient).Ping);
                        JSONPacket t_KeepAlivePacket = new JSONPacket("KEEP_ALIVE", t_KeepAliveData);
                        ((RemoteClient)la_RemoteClient).Send(t_KeepAlivePacket);
                        ((RemoteClient)la_RemoteClient).LastKeepAlive = t_Now;
                        ((RemoteClient)la_RemoteClient).ReceivedKeepAlive = false;
                        DebugLine("Sent keep alive to {0}, Ping: {1}", ((RemoteClient)la_RemoteClient).ClientID, ((RemoteClient)la_RemoteClient).Ping);
                        Thread.Sleep(KeepAlivePeriod);
                    }
                }
            }));

            while (true)
            {
                t_BytesRead = 0;

                try
                {
                    if (t_KeepAliveThread.ThreadState == ThreadState.Unstarted)
                        t_KeepAliveThread.Start(t_RemoteClient);

                    t_BytesRead = t_ClientStream.Read(t_Message, 0, 4096);
                }
                catch
                {
                    break;
                }

                if (t_BytesRead == 0)
                    break;

                String t_ReceivedPacket = Encoding.Unicode.GetString(new List<Byte>(t_Message).GetRange(0, t_BytesRead).ToArray());
                JSONPacket t_Packet = null;

                if (Utils.JSONValid(t_ReceivedPacket))
                    t_Packet = JSONPacket.FromJson(t_ReceivedPacket);

                if (t_Packet != null)
                {
                    if (t_Packet.PacketName == "KEEP_ALIVE")
                    {
                        DateTime t_PacketDate = new DateTime(Convert.ToInt64(t_Packet.Data["Time"]));

                        if (t_FirstKeepAlives > 3)
                        {
                            t_RemoteClient.Ping = (Single)(((Double)t_PacketDate.Ticks - (Double)t_RemoteClient.LastKeepAlive.Ticks) / 10000)/* - KeepAlivePeriod * 100000*/;
                            
                            if (t_RemoteClient.Ping < 0 || t_RemoteClient.Ping > Timeout)
                            {
                                DebugLine("Timeout {0}", t_RemoteClient.ClientID);
                                break;
                            }
                        }

                        t_RemoteClient.ReceivedKeepAlive = true;

                        DispatchKeepAlive(t_RemoteClient);
                    }
                    else
                    {
                        t_RemoteClient.LastPacket = t_Packet;
                        DispatchPacketReceive(t_RemoteClient, t_Packet);
                    }
                }
                else
                {
                    t_RemoteClient.LastMessage = t_ReceivedPacket;
                    DispatchMessageReceive(t_RemoteClient);
                }

                if (t_FirstKeepAlives < 4)
                    t_FirstKeepAlives++;
            }

            DispatchClientDisconnect(t_RemoteClient);
            
            DebugLine("Client disconnected");

            t_KeepAliveThread.Abort();
            t_TcpClient.Close();
        }
    }
}
