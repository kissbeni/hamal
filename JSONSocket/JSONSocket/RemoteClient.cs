﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Web.Script.Serialization;

namespace JSocket
{
    public class RemoteClient
    {
        public RemoteClient()
        {
            ClientID = DateTime.Now.Ticks;
        }

        public ClientStatus Status { get; internal set; }
        public String LastMessage { get; internal set; }
        public JSONPacket LastPacket { get; internal set; }
        public TcpClient TcpClient { get; internal set; }
        public NetworkStream TcpStream { get; internal set; }
        public Boolean ReceivedKeepAlive { get; internal set; }
        public Int64 ClientID { get; private set; }
        public DateTime LastKeepAlive { get; internal set; }
        public Single Ping { get; internal set; }

        public void Send(JSONPacket a_Packet)
        {
            if (a_Packet == null)
                return;

            SendMessage(a_Packet.Serialize());
        }

        public void SendMessage(String a_Message, Boolean a_Unicode = true)
        {
            Byte[] t_Bytes;

            if (a_Unicode)
                t_Bytes = Encoding.Unicode.GetBytes(a_Message);
            else
                t_Bytes = Encoding.ASCII.GetBytes(a_Message);

            if (t_Bytes.Length >= 4096)
                throw new Exception("Network stream owerflow!");

            g_Retry:

            try
            {
                TcpStream.Write(t_Bytes, 0, t_Bytes.Length);
                TcpStream.Flush();
            }
            catch
            {
                goto g_Retry;
            }
        }

        public void SendMessage(Dictionary<String, Object> a_Message, Boolean a_Unicode = true)
        {
            JavaScriptSerializer t_Serializer = new JavaScriptSerializer();
            SendMessage(t_Serializer.Serialize(a_Message), a_Unicode);
        }
    }

    public enum ClientStatus
    {
        CLIENT_STATE_NOW_CONNECTED,
        CLIENT_STATE_MESSAGE_SEND,
        CLIENT_STATE_NOW_DISCONNECTED
    }
}
