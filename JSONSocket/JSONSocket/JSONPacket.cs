﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace JSocket
{
    public partial class JSONPacket
    {
        public JSONPacket(String a_PacketName, Dictionary<String, Object> a_Data)
        {
            PacketName = a_PacketName;
            Data = a_Data;
        }

        public static JSONPacket FromJson(String a_Json)
        {
            if (!Utils.JSONValid(a_Json))
                throw new InvalidJsonException();

            try
            {
                String t_PacketName = "INVALID_PACKET";

                JavaScriptSerializer t_Serializer = new JavaScriptSerializer();
                Dictionary<String, Object> t_DeserializedJson = t_Serializer.Deserialize<Dictionary<string, object>>(a_Json);
                Dictionary<String, Object> t_DataJson = new Dictionary<string, object>();

                if (t_DeserializedJson.ContainsKey("Packet"))
                    t_PacketName = t_DeserializedJson["Packet"].ToString();

                if (t_DeserializedJson.ContainsKey("Data") && t_DeserializedJson["Data"] is IDictionary<string, object>)
                    t_DataJson = (Dictionary<string, object>)t_DeserializedJson["Data"];

                return new JSONPacket(t_PacketName, t_DataJson);
            }
            catch
            {
                return null;
            }
        }

        public String PacketName { get; private set; }
        public Dictionary<String, Object> Data { get; private set; }

        public string Serialize()
        {
            JavaScriptSerializer t_Serializer = new JavaScriptSerializer();

            Dictionary<String, Object> t_Data = new Dictionary<String, Object>();
            t_Data.Add("Packet", PacketName);
            t_Data.Add("Data", Data);

            return t_Serializer.Serialize(t_Data);
        }
    }
}
