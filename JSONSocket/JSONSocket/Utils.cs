﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JSocket
{
    public static class Utils
    {
        public static Boolean JSONValid(String a_Json)
        {
            Int32 c_bracket_type_0 = 0;
            Int32 c_bracket_type_1 = 0;
            Int32 c_bracket_type_2 = 0;
            Int32 c_bracket_type_3 = 0;
            Int32 c_string_begin = 0;

            foreach (Char t_Char in a_Json)
            {
                if (t_Char != '{' && t_Char != '}' && t_Char != '[' && t_Char != ']' && t_Char != '"')
                    continue;

                if (t_Char == '{' && (c_string_begin == 0 || (c_string_begin > 0 && c_string_begin % 2 == 0)))
                    c_bracket_type_0++;
                else if (t_Char == '}' && (c_string_begin == 0 || (c_string_begin > 0 && c_string_begin % 2 == 0)))
                    c_bracket_type_1++;
                else if (t_Char == '[' && (c_string_begin == 0 || (c_string_begin > 0 && c_string_begin % 2 == 0)))
                    c_bracket_type_2++;
                else if (t_Char == ']' && (c_string_begin == 0 || (c_string_begin > 0 && c_string_begin % 2 == 0)))
                    c_bracket_type_3++;
                else if (t_Char == '"')
                    c_string_begin++;
            }

            return (c_string_begin % 2 == 0 && c_bracket_type_0 == c_bracket_type_1 && c_bracket_type_2 == c_bracket_type_3);
        }

        internal static void WriteJSONDictionary(BinaryWriter a_Writer, Encoding a_Encoding, Dictionary<String, Object> a_Data)
        {
            foreach (KeyValuePair<String, Object> t_Pair in a_Data)
            {
                a_Writer.Write(a_Encoding.GetBytes(t_Pair.Key));
                a_Writer.Write((Byte)0);

                ProcessObject(a_Writer, a_Encoding, t_Pair.Value);
            }
        }

        public static void ProcessObject(BinaryWriter a_Writer, Encoding a_Encoding, Object a_Object)
        {
            if (a_Object is Dictionary<String, Object>)
            {
                a_Writer.Write((Byte)JSONPacketField.JPF_DATA);
                a_Writer.Write((a_Object as Dictionary<String, Object>).Count);

                WriteJSONDictionary(a_Writer, a_Encoding, a_Object as Dictionary<String, Object>);
            }
            else if (a_Object is Array || a_Object is ArrayList)
            {
                a_Writer.Write((Byte)JSONPacketField.JPF_ARRAY);

                dynamic t_Array = a_Object;

                a_Writer.Write(t_Array.Length);

                foreach (Object t_Object in t_Array)
                    ProcessObject(a_Writer, a_Encoding, t_Object);
            }
            else if (a_Object is String)
            {
                a_Writer.Write((Byte)JSONPacketField.JPF_STRING);

                String t_Str = a_Object as String;

                a_Writer.Write(t_Str.Length);
                a_Writer.Write(a_Encoding.GetBytes(t_Str));
            }
            else
            {
                a_Writer.Write((Byte)JSONPacketField.JPF_NUMBER);

                if (a_Object is Boolean)
                {
                    a_Writer.Write((Byte)JSONPacketNumber.JPN_BOOLEAN);
                    a_Writer.Write((Boolean)a_Object);
                }
                else if (a_Object is Char)
                {
                    a_Writer.Write((Byte)JSONPacketNumber.JPN_CHAR);
                    a_Writer.Write((Char)a_Object);
                }
                else if (a_Object is Byte)
                {
                    a_Writer.Write((Byte)JSONPacketNumber.JPN_BYTE);
                    a_Writer.Write((Byte)a_Object);
                }
                else if (a_Object is Int16)
                {
                    a_Writer.Write((Byte)JSONPacketNumber.JPN_INT16);
                    a_Writer.Write((Int16)a_Object);
                }
                else if (a_Object is UInt16)
                {
                    a_Writer.Write((Byte)JSONPacketNumber.JPN_UINT16);
                    a_Writer.Write((UInt16)a_Object);
                }
                else if (a_Object is Int32)
                {
                    a_Writer.Write((Byte)JSONPacketNumber.JPN_INT32);
                    a_Writer.Write((Int32)a_Object);
                }
                else if (a_Object is UInt32)
                {
                    a_Writer.Write((Byte)JSONPacketNumber.JPN_UINT32);
                    a_Writer.Write((UInt32)a_Object);
                }
                else if (a_Object is Int64)
                {
                    a_Writer.Write((Byte)JSONPacketNumber.JPN_INT64);
                    a_Writer.Write((Int64)a_Object);
                }
                else if (a_Object is UInt64)
                {
                    a_Writer.Write((Byte)JSONPacketNumber.JPN_UINT64);
                    a_Writer.Write((UInt64)a_Object);
                }
                else if (a_Object is Single)
                {
                    a_Writer.Write((Byte)JSONPacketNumber.JPN_SINGLE);
                    a_Writer.Write((Single)a_Object);
                }
                else if (a_Object is Double)
                {
                    a_Writer.Write((Byte)JSONPacketNumber.JPN_DOUBLE);
                    a_Writer.Write((Double)a_Object);
                }
                else
                    throw new Exception("Invalid type: " + a_Object.GetType().FullName);
            }
        }

        internal enum JSONPacketField : byte
        {
            //enum name    structure in pkt
            JPF_NUMBER,  // [number_type][number]
            JPF_STRING,  // [length][string]
            JPF_ARRAY,   // [length][item1 (with field type(s) too)]...
            JPF_DATA     // a json dictionary wich can contain these options
        }

        internal enum JSONPacketNumber : byte
        {
            JPN_BOOLEAN,
            JPN_CHAR,
            JPN_BYTE,
            JPN_INT16,
            JPN_UINT16,
            JPN_INT32,
            JPN_UINT32,
            JPN_INT64,
            JPN_UINT64,
            JPN_SINGLE,
            JPN_DOUBLE
        }
    }
}
