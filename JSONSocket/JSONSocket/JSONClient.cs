﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;

namespace JSocket
{
    public class JSONClient
    {
        #region Events
        public delegate void ClientEventHandler(JSONClient a_Client);
        public delegate void ClientPacketEventHandler(JSONClient a_Client, JSONPacket a_Packet);
        public event ClientEventHandler OnClientConnect;
        public event ClientEventHandler OnClientDisconnect;
        public event ClientEventHandler OnMessageReceive;
        public event ClientEventHandler OnKeepAlive;
        public event ClientPacketEventHandler OnPacketReceive;

        private void DispatchClientConnect()
        {
            if (OnClientConnect != null)
                OnClientConnect(this);
        }

        private void DispatchClientDisconnect()
        {
            if (OnClientDisconnect != null)
                OnClientDisconnect(this);
        }

        private void DispatchMessageReceive()
        {
            if (OnMessageReceive != null)
                OnMessageReceive(this);
        }

        private void DispatchKeepAlive()
        {
            if (OnKeepAlive != null)
                OnKeepAlive(this);
        }

        private void DispatchPacketReceive(JSONPacket a_Packet)
        {
            if (OnPacketReceive != null)
                OnPacketReceive.BeginInvoke(this, a_Packet, null, null);
        }
        #endregion

        public String LastMessage { get; internal set; }
        public JSONPacket LastPacket { get; internal set; }
        public TcpClient TcpClient { get; internal set; }
        public NetworkStream TcpStream { get; internal set; }
        public Boolean ReceivedKeepAlive { get; internal set; }
        public Int64 ClientID { get; private set; }
        public DateTime LastKeepAlive { get; internal set; }
        public Single Ping { get; internal set; }
        
        /// <summary>
        /// The server's port
        /// </summary>
        public UInt16 Port { get; private set; }
        
        /// <summary>
        /// Server status
        /// </summary>
        public Boolean Online { get; private set; }

        /// <summary>
        /// Allow debug messages
        /// </summary>
        public Boolean Debug { get; set; }

        private Int32 m_KeepAlivePeriod = 5000;

        public JSONClient(String a_IPAddress, UInt16 a_Port = 32)
        {
            Init(IPAddress.Parse(a_IPAddress), a_Port);
        }

        public JSONClient(IPAddress a_IPAddress, UInt16 a_Port = 32)
        {
            Init(a_IPAddress, a_Port);
        }

        private void Init(IPAddress a_IPAddress, UInt16 a_Port)
        {
            Port = a_Port;

            TcpClient = new TcpClient();

            IPEndPoint serverEndPoint = new IPEndPoint(a_IPAddress, a_Port);

            TcpClient.Connect(serverEndPoint);

            TcpStream = TcpClient.GetStream();
        }

        private void DebugLine(String a_Format, params Object[] a_Args)
        {
            if (!Debug)
                return;

            Console.WriteLine(a_Format, a_Args);
        }

        /// <summary>
        /// Starts the listener loop
        /// </summary>
        /// <param name="a_Async">If true, the server will run in an other thread.</param>
        public void Start(Boolean a_Async = false)
        {
            if (Online)
            {
                DebugLine("You can't start the server, because its already running!");
                return;
            }

            DebugLine("Starting server...");

            Online = true;

            if (!a_Async)
                Listen();
            else
                new Thread(new ThreadStart(Listen)).Start();
        }

        [Obsolete]
        public void Stop(Boolean a_Close)
        {
            Stop();
        }

        /// <summary>
        /// Stops the listener loop
        /// </summary>
        public void Stop()
        {
            Online = false;

            TcpClient.Close();
        }

        /// <summary>
        /// Send a JSON packet to the server
        /// </summary>
        /// <param name="a_Packet">The JSON packet</param>
        public void Send(JSONPacket a_Packet)
        {
            new Thread(new ThreadStart(() => SendMessage(a_Packet.Serialize()))).Start();
        }

        /// <summary>
        /// Send a raw string to the server
        /// </summary>
        /// <param name="a_Message">The message</param>
        /// <param name="a_Unicode">Send as unicode</param>
        public void SendMessage(String a_Message, Boolean a_Unicode = true)
        {
            byte[] bytes;

            if (a_Unicode)
                bytes = Encoding.Unicode.GetBytes(a_Message);
            else
                bytes = Encoding.ASCII.GetBytes(a_Message);

            retry:
            try
            {
                TcpStream.Write(bytes, 0, bytes.Length);
                TcpStream.Flush();
            }
            catch
            {
                goto retry;
            }
        }

        /// <summary>
        /// Serialize a dictionary and send as a JSON string
        /// </summary>
        /// <param name="a_Message">The packet content</param>
        /// <param name="a_Unicode">Send as unicode</param>
        public void SendMessage(Dictionary<String, Object> a_Message, Boolean a_Unicode = true)
        {
            JavaScriptSerializer t_Serializer = new JavaScriptSerializer();
            SendMessage(t_Serializer.Serialize(a_Message), a_Unicode);
        }

        private void Listen()
        {
            DebugLine("Client loop start");
            DebugLine("Sending first keep alive");

            DateTime t_Now = DateTime.Now;
            LastKeepAlive = t_Now;

            Dictionary<String, Object> t_KeepAliveData = new Dictionary<string, object>();
            t_KeepAliveData.Add("Time", t_Now.Ticks);
            Send(new JSONPacket("KEEP_ALIVE", t_KeepAliveData));

            while (Online)
            {
                if (!TcpStream.CanRead)
                    break;

                Byte[] t_Message = new Byte[4096];
                Int32 t_BytesRead;

                try
                {
                    t_BytesRead = TcpStream.Read(t_Message, 0, 4096);
                }
                catch
                {
                    break;
                }

                String t_Packet = Encoding.Unicode.GetString(new List<Byte>(t_Message).GetRange(0, t_BytesRead).ToArray());

                JSONPacket t_ReceivedPacket = null;

                if (Utils.JSONValid(t_Packet))
                    t_ReceivedPacket = JSONPacket.FromJson(t_Packet);

                if (t_ReceivedPacket != null)
                {
                    if (t_ReceivedPacket.PacketName == "KEEP_ALIVE")
                    {
                        LastKeepAlive = DateTime.Now;

                        t_KeepAliveData = new Dictionary<String, Object>();
                        t_KeepAliveData.Add("Time", LastKeepAlive.Ticks);
                        JSONPacket t_Answer = new JSONPacket("KEEP_ALIVE", t_KeepAliveData);

                        Ping = Convert.ToSingle(t_ReceivedPacket.Data["Ping"]);
                        
                        if (t_ReceivedPacket.Data.ContainsKey("ClientID"))
                            ClientID = Convert.ToInt64(t_ReceivedPacket.Data["ClientID"]);

                        if (t_ReceivedPacket.Data.ContainsKey("KeepAlivePeroid"))
                            m_KeepAlivePeriod = Convert.ToInt32(t_ReceivedPacket.Data["KeepAlivePeroid"]);

                        Send(t_Answer);

                        DebugLine("Sent keep alive to server, Ping: {0}", Ping);

                        DispatchKeepAlive();
                    }
                    else
                    {
                        LastPacket = t_ReceivedPacket;
                        DispatchPacketReceive(t_ReceivedPacket);
                    }
                }
                else
                {
                    LastMessage = t_Packet;
                    DispatchMessageReceive();
                }
            }

            DebugLine("Client loop end");

            Online = false;
        }
    }
}
