﻿using JSocket;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            JSONServer s = new JSONServer();
            //s.Debug = true;
            s.Timeout = Int32.MaxValue;
            s.OnMessageReceive += (a, b) =>
            {
                Console.WriteLine("Message got from {0}: {1}", a.ClientID, a.LastMessage);
            };
            s.Start(true);

            Thread.Sleep(200);

            JSONClient c = new JSONClient("127.0.0.1");
            //c.Debug = true;
            c.Start(true);
            while (true)
            {
                c.SendMessage(DateTime.Now.ToLongTimeString());
                Thread.Sleep(1000);
            }
        }
    }
}
