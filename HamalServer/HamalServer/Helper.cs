﻿using DataStorage;
using JSocket;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamal.Server
{
    [Flags]
    public enum AuthenticationState
    {
        LOGIN_OK = 0,
        LOGIN_USERNAME_BAD = 1,
        LOGIN_PASSWORD_BAD = 2,
        LOGIN_SERVICE_BUSY = 4
    }

    public enum UserStatus
    {
        ONLINE,
        BUSY,
        AWAY,
        HIDDEN,
        OFFLINE
    }

    public class AuthenticationInfo
    {
        public AuthenticationState State { get; set; }
        public string DisplayName { get; set; }
        public UUID UUID { get; set; }
        public UserStatus Status { get; set; }
        public string StatusMessage { get; set; }
        public long DatabaseID { get; set; }

        public static AuthenticationInfo Default
        {
            get
            {
                AuthenticationInfo info = new AuthenticationInfo();
                info.State = AuthenticationState.LOGIN_SERVICE_BUSY;
                info.DisplayName = "Unknown";
                info.UUID = new UUID();
                info.Status = UserStatus.OFFLINE;
                info.StatusMessage = "";
                info.DatabaseID = -1;

                return info;
            }
        }
    }

    public static class Helper
    {
        public static String ToUTF8(this Byte[] data)
        {
            return Encoding.UTF8.GetString(data);
        }

        public static Byte[] FromUTF8(this String data)
        {
            return Encoding.UTF8.GetBytes(data);
        }

        public static Int64 ToInt64(this Byte[] data)
        {
            return BitConverter.ToInt64(data, 0);
        }

        public static Tkey GetKey<Tkey, Tval>(this Dictionary<Tkey, Tval> src, Tval val)
        {
            foreach (KeyValuePair<Tkey, Tval> kvp in src)
                if (kvp.Value.Equals(val))
                    return kvp.Key;

            return default(Tkey);
        }

        public static Dictionary<RemoteClient, AuthenticationInfo> Clients = new Dictionary<RemoteClient, AuthenticationInfo>();
        public static Dictionary<Int64, String> DataBaseIDBindings = new Dictionary<Int64, String>();

        public static DataFile GetUserData(Int64 dbid)
        {
            if (!File.Exists("Users\\" + dbid + ".hdf"))
                return null;

            return new DataFile("Users\\" + dbid + ".hdf");
        }

        public static DataFile GetUserData(String username)
        {
            if (!DataBaseIDBindings.ContainsValue(username))
                return null;

            return GetUserData(DataBaseIDBindings.GetKey(username));
        }

        public static Byte[] Int64Array(Int64[] items)
        {
            using (MemoryStream s = new MemoryStream())
            {
                using (BinaryWriter w = new BinaryWriter(s))
                {
                    w.Write(items.Length);

                    foreach (Int64 i in items)
                        w.Write(i);
                }

                return s.ToArray();
            }
        }

        public static Int64[] Int64Array(Byte[] data)
        {
            if (data.Length < 4)
                return new Int64[0];

            using (MemoryStream s = new MemoryStream(data))
            {
                List<Int64> ret = new List<Int64>();
                using (BinaryReader r = new BinaryReader(s))
                {
                    Int32 len = r.ReadInt32();

                    for (Int32 i = 0; i < len; i++)
                        ret.Add(r.ReadInt64());
                }

                return ret.ToArray();
            }
        }

        public static AuthenticationInfo Authenticate(string Username, string Password)
        {
            AuthenticationInfo ret = new AuthenticationInfo();
            AuthenticationState AS = AuthenticationState.LOGIN_OK;

            DataFile data = GetUserData(Username);

            if (data != null)
            {
                String ads = data.GetEntry("USER_LOGIN_DATA", "Password").ToUTF8();
                if (Password != ads)
                    AS |= AuthenticationState.LOGIN_PASSWORD_BAD;

                if (AS == AuthenticationState.LOGIN_OK)
                {
                    ret.DisplayName = data.GetEntry("USER_DATA", "DisplayName").ToUTF8();
                    ret.UUID = new UUID(data.GetEntry("USER_DATA", "UUID"));
                    ret.DatabaseID = data.GetEntry("USER_DATA", "DatabaseID").ToInt64();
                    ret.Status = (UserStatus)data.GetEntry("USER_DATA", "Status")[0];
                    ret.StatusMessage = data.GetEntry("USER_DATA", "StatusMessage").ToUTF8();
                }
            }
            else
                AS |= AuthenticationState.LOGIN_USERNAME_BAD;

            ret.State = AS;

            return ret;
        }

        public static Int64[] GetContacts(AuthenticationInfo user)
        {
            if (user == null)
                return new Int64[0];

            DataFile data = GetUserData(user.DatabaseID);

            if (data != null)
                return Int64Array(data.GetEntry("USER_DATA", "Contacts"));

            return new Int64[0];
        }

        public static AuthenticationInfo GetUserByDBID(long dbid)
        {
            foreach (AuthenticationInfo a in Clients.Values)
                if (a.DatabaseID == dbid)
                    return a;

            DataFile data = GetUserData(dbid);

            if (data != null)
            {
                AuthenticationInfo ret = new AuthenticationInfo();
                ret.DisplayName = data.GetEntry("USER_DATA", "DisplayName").ToUTF8();
                ret.UUID = new UUID(data.GetEntry("USER_DATA", "UUID"));
                ret.DatabaseID = data.GetEntry("USER_DATA", "DatabaseID").ToInt64();
                ret.Status = UserStatus.OFFLINE;
                ret.StatusMessage = data.GetEntry("USER_DATA", "StatusMessage").ToUTF8();

                return ret;
            }

            AuthenticationInfo fakeRet = AuthenticationInfo.Default;
            fakeRet.DatabaseID = dbid;
            return fakeRet;
        }

        public static String GetDisplayName(Int64 dbid)
        {
            DataFile data = GetUserData(dbid);

            if (data != null)
                return data.GetEntry("USER_DATA", "DisplayName").ToUTF8();

            return "UNKNOWN_USER_DISPLAYNAME";
        }

        public static void SaveClient(AuthenticationInfo info)
        {
            DataFile f = new DataFile("Users\\" + info.DatabaseID + ".hdf");
            f.AddEntry("USER_DATA", "DisplayName", Encoding.UTF8.GetBytes(info.DisplayName));
            f.AddEntry("USER_DATA", "UUID", info.UUID.ToBytes());
            f.AddEntry("USER_DATA", "Status", new Byte[] { (Byte)info.Status });
            f.AddEntry("USER_DATA", "StatusMessage", Encoding.UTF8.GetBytes(info.StatusMessage));
            f.AddEntry("USER_DATA", "DatabaseID", BitConverter.GetBytes(info.DatabaseID));
            f.AddEntry("USER_DATA", "ProfilePicture", File.ReadAllBytes("D:\\programming\\5fd80d98f96f9adbde7876cbda181715.png"));
            f.AddEntry("USER_DATA", "Contacts", Helper.Int64Array(GetContacts(info)));
            f.Save();
        }

        public static RemoteClient GetClientByDBID(long dbid)
        {
            foreach (KeyValuePair<RemoteClient, AuthenticationInfo> a in Clients)
                if (a.Value != null && a.Value.DatabaseID == dbid)
                    return a.Key;

            return null;
        }

        public static string ToBase64(this string s)
        {
            if (s == null)
                return null;

            return Convert.ToBase64String(Encoding.Default.GetBytes(s));
        }

        public static string FromBase64(this string s)
        {
            if (s == null)
                return null;

            return Encoding.Default.GetString(Convert.FromBase64String(s));
        }

        public static long GetUserDBID(long p)
        {
            foreach (KeyValuePair<RemoteClient, AuthenticationInfo> a in Clients)
                if (a.Key != null && a.Key.ClientID == p)
                    return a.Value.DatabaseID;

            return -1;
        }
    }
}
