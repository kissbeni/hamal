﻿using DataStorage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Hamal.Server
{
    public class Chat
    {
        public static Dictionary<String, ChatRoom> m_Rooms = new Dictionary<String, ChatRoom>();

        public static ChatRoom CreateRoom(String t_Nane, List<ChatMember> t_Members)
        {
            if (t_Nane == null || t_Nane.Length < 2)
                return null;

            ChatRoom t_Room = new ChatRoom();
            t_Room.Name = t_Nane;
            t_Room.Members.AddRange(t_Members);
            t_Room.UID = new UUID((Byte)t_Nane[0]);
            m_Rooms.Add(t_Room.UID, t_Room);
            SaveRoom(t_Room);

            Console.WriteLine("[CHAT] RoomCreated:");
            Console.WriteLine("       Name: {0}", t_Nane);
            Console.WriteLine("       Member count: {0}", t_Room.Members.Count);
            Console.WriteLine("       UID: {0}", t_Room.UID);

            return t_Room;
        }

        public static void CreateRoom(DataFile t_Data)
        {
            ChatRoom t_Room = new ChatRoom();
            t_Room.Name = t_Data.GetEntry("ROOM_BASIC_INFO", "Name").ToUTF8();
            t_Room.Password = t_Data.GetEntry("ROOM_BASIC_INFO", "Password").ToUTF8();
            t_Room.UID = t_Data.GetEntry("ROOM_BASIC_INFO", "UID").ToUTF8();

            UUID.ForceResolve(t_Room.UID);

            t_Room.IsPrivate = BitConverter.ToBoolean(t_Data.GetEntry("ROOM_BASIC_INFO", "IsPrivate"), 0);
            t_Room.DefaultPermissions = (ChatPermission)BitConverter.ToUInt32(t_Data.GetEntry("ROOM_BASIC_INFO", "DefaultPermissions"), 0);

            t_Room.History = new List<ChatEntry>();
            using (BinaryReader t_HistoryDataReader = new BinaryReader(new MemoryStream(t_Data.GetEntry("ROOM_PAST", "History"))))
            {
                Int32 t_Count = t_HistoryDataReader.ReadInt32();

                for (Int32 t_Index = 0; t_Index < t_Count; t_Index++)
                {
                    ChatEntry t_Entry = new ChatEntry();
                    t_Entry.Sender = t_HistoryDataReader.ReadInt64();
                    t_Entry.Timestamp = new DateTime(t_HistoryDataReader.ReadInt64());
                    t_Entry.MessageStack = new List<String>();

                    Int32 t_Count2 = t_HistoryDataReader.ReadInt32();

                    for (Int32 t_Index2 = 0; t_Index2 < t_Count2; t_Index2++)
                        t_Entry.MessageStack.Add(t_HistoryDataReader.ReadString());

                    t_Room.History.Add(t_Entry);
                }
            }

            t_Room.Members = new List<ChatMember>();
            using (BinaryReader t_MembersDataReader = new BinaryReader(new MemoryStream(t_Data.GetEntry("ROOM_PAST", "Members"))))
            {
                Int32 t_Count = t_MembersDataReader.ReadInt32();

                for (Int32 t_Index = 0; t_Index < t_Count; t_Index++)
                {
                    ChatMember t_Member = new ChatMember();
                    t_Member.DatabaseID = t_MembersDataReader.ReadInt64();
                    t_Member.JoinTime = new DateTime(t_MembersDataReader.ReadInt64());
                    t_Member.Permissions = (ChatPermission)t_MembersDataReader.ReadUInt32();

                    t_Room.Members.Add(t_Member);
                }
            }

            m_Rooms.Add(t_Room.UID, t_Room);

            Console.WriteLine("[CHAT] RoomCreated:");
            Console.WriteLine("       Name: {0}", t_Room.Name);
            Console.WriteLine("       Member count: {0}", t_Room.Members.Count);
            Console.WriteLine("       UID: {0}", t_Room.UID);
        }

        public static void SaveRoom(ChatRoom t_Room)
        {
            DataFile t_Data = new DataFile("Rooms\\" + t_Room.UID + ".hdf");

            t_Data.AddEntry("ROOM_BASIC_INFO", "Name", t_Room.Name.FromUTF8());
            
            if (t_Room.Password != null)
                t_Data.AddEntry("ROOM_BASIC_INFO", "Password", t_Room.Password.FromUTF8());
            else
                t_Data.AddEntry("ROOM_BASIC_INFO", "Password", new Byte[0]);

            t_Data.AddEntry("ROOM_BASIC_INFO", "UID", t_Room.UID.FromUTF8());
            t_Data.AddEntry("ROOM_BASIC_INFO", "IsPrivate", BitConverter.GetBytes(t_Room.IsPrivate));
            t_Data.AddEntry("ROOM_BASIC_INFO", "DefaultPermissions", BitConverter.GetBytes((UInt32)t_Room.DefaultPermissions));

            using (MemoryStream t_MemoryStream = new MemoryStream())
            {
                using (BinaryWriter t_HistoryDataWriter = new BinaryWriter(t_MemoryStream))
                {
                    t_HistoryDataWriter.Write(t_Room.History.Count);

                    foreach (ChatEntry t_Entry in t_Room.History)
                    {
                        t_HistoryDataWriter.Write(t_Entry.Sender);
                        t_HistoryDataWriter.Write(t_Entry.Timestamp.Ticks);
                        t_HistoryDataWriter.Write(t_Entry.MessageStack.Count);

                        foreach (String t_Str in t_Entry.MessageStack)
                            t_HistoryDataWriter.Write(t_Str);
                    }
                }

                t_Data.AddEntry("ROOM_PAST", "History", t_MemoryStream.ToArray());
            }

            using (MemoryStream t_MemoryStream = new MemoryStream())
            {
                using (BinaryWriter t_MembersDataWriter = new BinaryWriter(t_MemoryStream))
                {
                    t_MembersDataWriter.Write(t_Room.Members.Count);

                    foreach (ChatMember m in t_Room.Members)
                    {
                        t_MembersDataWriter.Write(m.DatabaseID);
                        t_MembersDataWriter.Write(m.JoinTime.Ticks);
                        t_MembersDataWriter.Write((UInt32)m.Permissions);
                    }
                }

                t_Data.AddEntry("ROOM_PAST", "Members", t_MemoryStream.ToArray());
            }

            t_Data.Save();
        }

        public static List<ChatRoom> GetChatRoomsForUser(Int64 a_DBID)
        {
            List<ChatRoom> t_Return = new List<ChatRoom>();

            foreach (ChatRoom t_Room in m_Rooms.Values)
                if (t_Room.HasUser(a_DBID))
                    t_Return.Add(t_Room);

            Console.WriteLine("[CHAT] GetChatRoomsForUser DatabaseID: {0}, Found {1} item(s).", a_DBID, t_Return.Count);

            return t_Return;
        }

        public static Boolean DoesChatRoomExist(String a_UID)
        {
            if (!UUID.IsResolved(a_UID))
                return false;

            return m_Rooms.ContainsKey(a_UID);
        }

        public static ChatRoom GetRoom(String a_UID)
        {
            if (!DoesChatRoomExist(a_UID))
                return null;

            return m_Rooms[a_UID];
        }

        public static List<ChatRoom> GetRoomsByName(String a_Name)
        {
            List<ChatRoom> t_Return = new List<ChatRoom>();

            foreach (ChatRoom t_Room in m_Rooms.Values)
                if (t_Room.Name == a_Name)
                    t_Return.Add(t_Room);

            Console.WriteLine("[CHAT] GetRoomsByName name: {0}, Found {1} item(s).", a_Name, t_Return.Count);
            return t_Return;
        }

        public static void UpdateRoom(ChatRoom a_Room)
        {
            if (DoesChatRoomExist(a_Room.UID))
            {
                m_Rooms[a_Room.UID] = a_Room;
                SaveRoom(m_Rooms[a_Room.UID]);
                Console.WriteLine("[CHAT] Room updated {0}[UID: {1}", a_Room.Name, a_Room.UID);
            }
        }

        public static ChatRoom CopyRoom(ChatRoom a_Room, bool a_IgnorePasswordAndPermissions = true)
        {
            ChatRoom t_Return = new ChatRoom();
            t_Return.DefaultPermissions = a_Room.DefaultPermissions;
            t_Return.IsPrivate = a_Room.IsPrivate;
            t_Return.Name = a_Room.Name;
            t_Return.UID = a_Room.UID;

            if (!a_IgnorePasswordAndPermissions)
                t_Return.Password = a_Room.Password;

            foreach (ChatEntry t_SrcEntry in a_Room.History)
            {
                ChatEntry t_DstEntry = new ChatEntry();
                t_DstEntry.MessageStack.AddRange(t_SrcEntry.MessageStack);
                t_DstEntry.Sender = t_SrcEntry.Sender;
                t_DstEntry.Timestamp = t_SrcEntry.Timestamp;
                t_Return.History.Add(t_DstEntry);
            }

            foreach (ChatMember t_SrcMember in a_Room.Members)
            {
                ChatMember t_DstMember = new ChatMember();
                t_DstMember.DatabaseID = t_SrcMember.DatabaseID;
                t_DstMember.JoinTime = t_SrcMember.JoinTime;

                if (!a_IgnorePasswordAndPermissions)
                    t_DstMember.Permissions = t_SrcMember.Permissions;
                else
                    t_DstMember.Permissions = t_Return.DefaultPermissions;
            }

            return t_Return;
        }

        public static ChatRoom SerializeRoom(Dictionary<String, Object> a_RoomData)
        {
            ChatRoom t_Return = new ChatRoom();
            t_Return.UID = a_RoomData["UID"].ToString();
            t_Return.Password = a_RoomData["PASSWORD"].ToString();
            t_Return.Name = a_RoomData["NAME"].ToString();
            t_Return.IsPrivate = a_RoomData["IS_PRIVATE"].Equals(1);
            t_Return.DefaultPermissions = (ChatPermission)Convert.ToInt32(a_RoomData["DEFAULT_PERMISSIONS"]);
            t_Return.History = ((ArrayList)a_RoomData["HISTORY"]).Cast<Dictionary<string, object>>().Select(x => SerializeChatEntry(x)).ToList();
            t_Return.Members = ((ArrayList)a_RoomData["MEMBERS"]).Cast<Dictionary<string, object>>().Select(x => SerializeChatMember(x)).ToList();
            return t_Return;
        }

        public static ChatEntry SerializeChatEntry(Dictionary<String, Object> t_Room)
        {
            ChatEntry t_Return = new ChatEntry();
            t_Return.Sender = Convert.ToInt64(t_Room["SENDER"]);
            t_Return.Timestamp = new DateTime(Convert.ToInt64(t_Room["TIMESTAMP"]));
            t_Return.MessageStack = ((ArrayList)t_Room["MESSAGE_STACK"]).Cast<string>().ToList();
            return t_Return;
        }

        public static ChatMember SerializeChatMember(Dictionary<String, Object> t_ChatMemberData)
        {
            ChatMember t_Return = new ChatMember();
            t_Return.DatabaseID = Convert.ToInt64(t_ChatMemberData["DBID"]);
            t_Return.JoinTime = new DateTime(Convert.ToInt64(t_ChatMemberData["JOIN_TIME"]));
            t_Return.Permissions = (ChatPermission)Convert.ToInt32(t_ChatMemberData["PERMISSIONS"]);
            return t_Return;
        }

        public static Dictionary<String, Object> SerializeRoom(ChatRoom t_Room, Boolean a_PublicDataOnly = false)
        {
            Dictionary<String, Object> t_Return = new Dictionary<String, Object>();
            t_Return.Add("UID", t_Room.UID);
            t_Return.Add("NAME", t_Room.Name);
            t_Return.Add("IS_PRIVATE", Convert.ToInt32(t_Room.IsPrivate));
            t_Return.Add("DEFAULT_PERMISSIONS", t_Room.DefaultPermissions);

            if (!a_PublicDataOnly)
            {
                t_Return.Add("PASSWORD", t_Room.Password);
                t_Return.Add("HISTORY", t_Room.History.Select(x => SerializeChatEntry(x)).ToArray());
                t_Return.Add("MEMBERS", t_Room.Members.Select(x => SerializeChatMember(x)).ToArray());
            }

            return t_Return;
        }

        public static Dictionary<String, Object> SerializeChatEntry(ChatEntry a_ChatEntry)
        {
            Dictionary<string, object> t_Return = new Dictionary<string, object>();
            t_Return.Add("SENDER", a_ChatEntry.Sender);
            t_Return.Add("TIMESTAMP", a_ChatEntry.Timestamp.Ticks);
            t_Return.Add("MESSAGE_STACK", a_ChatEntry.MessageStack.ToArray());
            return t_Return;
        }

        public static Dictionary<String, Object> SerializeChatMember(ChatMember a_ChatMember)
        {
            Dictionary<String, Object> t_Return = new Dictionary<String, Object>();
            t_Return.Add("DBID", a_ChatMember.DatabaseID);
            t_Return.Add("JOIN_TIME", a_ChatMember.JoinTime.Ticks);
            t_Return.Add("PERMISSIONS", a_ChatMember.Permissions);
            return t_Return;
        }

        public static void LeaveRooms(Int64 t_DBID)
        {
            foreach (ChatRoom t_Room in m_Rooms.Values)
                if (t_Room.HasUser(t_DBID))
                    t_Room.Leave(t_DBID);
        }
    }

    public class ChatRoom
    {
        public String Name { get; set; }
        public String Password { get; set; }
        public Boolean IsPrivate { get; set; }
        public List<ChatMember> Members = new List<ChatMember>();
        public List<ChatEntry> History = new List<ChatEntry>();
        public String UID { get; set; }
        public ChatPermission DefaultPermissions { get; set; }

        public ChatRoom()
        {
            Name = "Unnamed room";
            IsPrivate = false;
            Password = null;
            UID = "";
        }

        public Boolean HasUser(Int64 a_DBID)
        {
            foreach (ChatMember t_Member in Members)
                if (t_Member.DatabaseID == a_DBID)
                    return true;

            return false;
        }

        public ChatMember GetMember(Int64 a_DBID)
        {
            foreach (ChatMember t_Member in Members)
                if (t_Member.DatabaseID == a_DBID)
                    return t_Member;

            return null;
        }

        public void SendMessage(String a_Message, Int64 a_Sender)
        {
            ChatMember t_Member = GetMember(a_Sender);

            if (t_Member != null)
            {
                DateTime t_Now = DateTime.Now;

                ChatEntry t_Last = History.LastOrDefault();

                if (t_Last != null && t_Last.Sender == t_Member.DatabaseID && t_Last.Timestamp.Year == t_Now.Year && t_Last.Timestamp.Month == t_Now.Month && t_Last.Timestamp.Day == t_Now.Day && t_Last.Timestamp.Hour == t_Now.Hour && t_Last.Timestamp.Minute == t_Now.Minute && t_Last.Timestamp.Second == t_Now.Second)
                    t_Last.MessageStack.Add(a_Message.ToBase64());
                else
                    History.Add(new ChatEntry()
                    {
                        MessageStack = new List<String>(new[]
                        {
                            a_Message.ToBase64()
                        }),
                        Sender = t_Member.DatabaseID,
                        Timestamp = t_Now
                    });

                Chat.SaveRoom(this);
            }
        }

        public void Join(Int64 a_Joiner, ChatPermission a_ChatPermission)
        {
            ChatMember t_Member = GetMember(a_Joiner);

            if (t_Member == null)
                Members.Add(new ChatMember()
                {
                    DatabaseID = a_Joiner,
                    Permissions = a_ChatPermission,
                    JoinTime = DateTime.Now
                });
            else
                t_Member.Permissions = a_ChatPermission;

            Chat.SaveRoom(this);
        }

        public void Leave(Int64 t_Leaver)
        {
            ChatMember t_Member = GetMember(t_Leaver);

            if (t_Member != null)
                Members.Remove(t_Member);
        }
    }

    public class ChatEntry
    {
        public Int64 Sender { get; set; }
        public DateTime Timestamp { get; set; }
        public List<String> MessageStack = new List<String>();
    }

    public class ChatMember
    {
        public long DatabaseID { get; set; }
        public DateTime JoinTime { get; set; }
        public ChatPermission Permissions { get; set; }
    }

    [Flags]
    public enum ChatPermission
    {
        READ = 1,
        WRITE = 2,
        INVITE = 4,
        KICK = 8,
        BAN = 16,
        DELETE_MESSAGE_OWN = 32,
        DELETE_MESSAGE_OTHERS = 64,
        DELETE_ROOM = 128
    }
}
