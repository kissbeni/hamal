﻿#if DEBUG
    #define CREATE_DEBUG_USERS
#endif

using DataStorage;
using JSocket;
using Hamal.Server.PacketProcessors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;

namespace Hamal.Server
{
    public class Program
    {
        public static void Main(String[] a_Args)
        {
            Console.WriteLine("Initializing...");

            if (!Directory.Exists("Users"))
                Directory.CreateDirectory("Users");

            if (!Directory.Exists("Rooms"))
                Directory.CreateDirectory("Rooms");

            #if CREATE_DEBUG_USERS
            if (!File.Exists("Users\\0.hdf"))
            {
                DataFile t_File = new DataFile("Users\\0.hdf");
                t_File.AddEntry("USER_DATA", "DisplayName", Encoding.UTF8.GetBytes("Superuser #main#"));
                t_File.AddEntry("USER_DATA", "UUID", new UUID().ToBytes());
                t_File.AddEntry("USER_DATA", "Status", new Byte[] { (Byte)UserStatus.OFFLINE });
                t_File.AddEntry("USER_DATA", "StatusMessage", Encoding.UTF8.GetBytes("Just do it!"));
                t_File.AddEntry("USER_DATA", "DatabaseID", BitConverter.GetBytes((Int64)0));
                t_File.AddEntry("USER_DATA", "ProfilePicture", File.ReadAllBytes("D:\\programming\\5fd80d98f96f9adbde7876cbda181715.png"));
                t_File.AddEntry("USER_DATA", "Contacts", Helper.Int64Array(new Int64[] { 1 }));
                t_File.AddEntry("USER_LOGIN_DATA", "Username", Encoding.UTF8.GetBytes("admin"));
                t_File.AddEntry("USER_LOGIN_DATA", "Password", Encoding.UTF8.GetBytes("1234"));
                t_File.Save();
            }

            if (!File.Exists("Users\\1.hdf"))
            {
                DataFile t_File = new DataFile("Users\\1.hdf");
                t_File.AddEntry("USER_DATA", "DisplayName", Encoding.UTF8.GetBytes("Almost superuser"));
                t_File.AddEntry("USER_DATA", "UUID", new UUID().ToBytes());
                t_File.AddEntry("USER_DATA", "Status", new Byte[] { (Byte)UserStatus.OFFLINE });
                t_File.AddEntry("USER_DATA", "StatusMessage", Encoding.UTF8.GetBytes("You did it!"));
                t_File.AddEntry("USER_DATA", "DatabaseID", BitConverter.GetBytes((Int64)1));
                t_File.AddEntry("USER_DATA", "ProfilePicture", File.ReadAllBytes("D:\\programming\\5fd80d98f96f9adbde7876cbda181715.png"));
                t_File.AddEntry("USER_DATA", "Contacts", Helper.Int64Array(new Int64[] { 0 }));
                t_File.AddEntry("USER_LOGIN_DATA", "Username", Encoding.UTF8.GetBytes("admin2"));
                t_File.AddEntry("USER_LOGIN_DATA", "Password", Encoding.UTF8.GetBytes("1234"));
                t_File.Save();
            }
            #endif

            if (Directory.Exists("Users"))
                foreach (String t_File in Directory.GetFiles("Users"))
                    if (t_File.EndsWith(".hdf"))
                        using (DataFile t_Data = new DataFile(t_File))
                            Helper.DataBaseIDBindings.Add(Int64.Parse(t_File.Split('\\').Last().Split('.').First()), t_Data.GetEntry("USER_LOGIN_DATA", "Username").ToUTF8());

            if (Directory.Exists("Rooms"))
                foreach (String t_File in Directory.GetFiles("Rooms"))
                    if (t_File.EndsWith(".hdf"))
                        using (DataFile t_Data = new DataFile(t_File))
                            Chat.CreateRoom(t_Data);

            Console.WriteLine("Done");

            JSONServer t_Server = new JSONServer();
            t_Server.Timeout = 5000;
            JavaScriptSerializer t_Serializer = new JavaScriptSerializer();
            t_Server.OnPacketReceive += (la_Sender, la_Server, la_Packet) =>
            {
                new Thread(new ThreadStart(() =>
                {
                    Console.WriteLine("Handling packet: " + la_Packet.PacketName + "\n  Data: " + t_Serializer.Serialize(la_Packet.Data));
                    try
                    {
                        if (la_Packet.PacketName == "CHAT")
                            la_Sender.Send(PacketProcessor.ProcessChat(la_Sender, la_Server, la_Packet));
                        else if (la_Packet.PacketName == "LOGIN")
                            PacketProcessor.ProcessLogin(la_Sender, la_Server, la_Packet);
                        else if (la_Packet.PacketName == "REQUEST_PAGE")
                            PacketProcessor.ProcessRequestPage(la_Sender, la_Server, la_Packet);
                        else if (la_Packet.PacketName == "GET_CONTACTS")
                            PacketProcessor.ProcessGetContacts(la_Sender, la_Server, la_Packet);
                        else if (la_Packet.PacketName == "GET_USER_BY_ID")
                            PacketProcessor.ProcessGetUserByID(la_Sender, la_Server, la_Packet);
                        else if (la_Packet.PacketName == "SET_USER_STATUS")
                            PacketProcessor.ProcessSetUserStatus(la_Sender, la_Server, la_Packet);
                        else if (la_Packet.PacketName == "SET_STATUS_MESSAGE")
                            PacketProcessor.ProcessSetStatusMessage(la_Sender, la_Server, la_Packet);
                        else if (la_Packet.PacketName == "GET_ICON")
                            PacketProcessor.ProcessGetIcon(la_Sender, la_Server, la_Packet);
                        else if (la_Packet.PacketName == "GET_DISPLAYNAME")
                            PacketProcessor.ProcessGetDisplayName(la_Sender, la_Server, la_Packet);
                    }
                    catch { }
                }))
                {
                    IsBackground = true
                }.Start();
            };

            t_Server.OnClientConnect += (la_Client, la_Server) =>
            {
                new Thread(new ThreadStart(() =>
                {
                    Console.WriteLine("Client connected: {0}", la_Client.ClientID);
                    Helper.Clients.Add(la_Client, null);

                    while (Helper.Clients.ContainsKey(la_Client) && Helper.Clients[la_Client] == null)
                    {
                        la_Client.SendMessage("CONNECTION_OK");
                        Console.WriteLine(la_Client.ClientID + " <- CONNECTION_OK");
                        Thread.Sleep(500);
                    }
                }))
                {
                    IsBackground = true
                }.Start();
            };

            t_Server.OnClientDisconnect += (la_Client, la_Server) =>
            {
                Console.WriteLine("Client disconnected: {0}", la_Client.ClientID);

                if (Helper.Clients.ContainsKey(la_Client))
                {
                    Int64[] t_Contacts = Helper.GetContacts(Helper.Clients[la_Client]);

                    AuthenticationInfo t_Info = Helper.Clients[la_Client];

                    if (t_Info == null)
                    {
                        Helper.Clients.Remove(la_Client);
                        return;
                    }

                    Chat.LeaveRooms(Helper.Clients[la_Client].DatabaseID);
                    t_Info.Status = UserStatus.OFFLINE;

                    foreach (Int64 t_Contact in t_Contacts)
                    {
                        RemoteClient t_Client = Helper.GetClientByDBID(t_Contact);

                        if (t_Client == null)
                            return;

                        t_Client.Send(new JSONPacket("USER_DATA_ANSWER", new Dictionary<String, Object>()
                        {
                            { "USER_DBID", t_Info.DatabaseID },
                            { "USER_INFO", new Dictionary<String, Object>()
                                {
                                    { "DISPLAY_NAME", t_Info.DisplayName.ToBase64() },
                                    { "UUID", t_Info.UUID },
                                    { "STATUS", t_Info.Status == UserStatus.HIDDEN ? UserStatus.OFFLINE : t_Info.Status },
                                    { "STATUS_MESSAGE", t_Info.StatusMessage.ToBase64() }
                                }
                            }
                        }));
                    }

                    Helper.Clients.Remove(la_Client);
                }
            };

            t_Server.Start();
        }
    }
}
