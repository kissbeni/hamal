﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Hamal.Server
{
    public class UUID
    {
        private static List<String> m_ResolvedUUIDs = new List<String>();
        private static Int64 m_UniqueHelper = 0;
        public Byte Seed { private set; get; }
        public DateTime CreationTime { private set; get; }
        public Boolean IsParsed { private set; get; }
        public Int16 PartHelper { private set; get; }
        public Int16 UUID_Part0 { private set; get; }
        public Int16 UUID_Part1 { private set; get; }
        public Int64 UUID_Part2 { private set; get; }
        public Int32 UUID_Part3 { private set; get; }
        public Byte[] UUID_Part0_Bytes { private set; get; }
        public Byte[] UUID_Part1_Bytes { private set; get; }
        public Byte[] UUID_Part2_Bytes { private set; get; }
        public Byte[] UUID_Part3_Bytes { private set; get; }

        public static implicit operator String(UUID a_UUID)
        {
            if (a_UUID == null)
                return null;

            return a_UUID.ToString();
        }

        public UUID()
        {
            Seed = 0;
            CreationTime = DateTime.Now;
            IsParsed = false;
            CreateUUID();
            m_ResolvedUUIDs.Add(this);
        }

        public UUID(Byte a_Seed)
        {
            Seed = a_Seed;
            CreationTime = DateTime.Now;
            IsParsed = false;
            CreateUUID();
            m_ResolvedUUIDs.Add(this);
        }

        public UUID(String a_UUID)
        {
            if (a_UUID.Split('-').Length != 4)
                goto g_InvalidUUID;

            String[] t_Parts = a_UUID.Split('-');
            
            Int16 t_Part0 = 0;
            Int16 t_Part1 = 0;
            Int64 t_Part2 = 0;
            Int32 t_Part3 = 0;

            for (Int32 t_Index = 0; t_Index < t_Parts.Length; t_Index++)
            {
                switch (t_Index)
                {
                    case 0:
                        if (!Int16.TryParse(t_Parts[t_Index], NumberStyles.HexNumber, CultureInfo.CurrentCulture, out t_Part0))
                            goto g_InvalidUUID;

                        break;

                    case 1:
                        if (!Int16.TryParse(t_Parts[t_Index], NumberStyles.HexNumber, CultureInfo.CurrentCulture, out t_Part1))
                            goto g_InvalidUUID;

                        break;

                    case 2:
                        if (!Int64.TryParse(t_Parts[t_Index], NumberStyles.HexNumber, CultureInfo.CurrentCulture, out t_Part2))
                            goto g_InvalidUUID;

                        break;

                    case 3:
                        if (!Int32.TryParse(t_Parts[t_Index], NumberStyles.HexNumber, CultureInfo.CurrentCulture, out t_Part3))
                            goto g_InvalidUUID;

                        break;
                }
            }

            UUID_Part0 = t_Part0;
            UUID_Part1 = t_Part1;
            UUID_Part2 = t_Part2;
            UUID_Part3 = t_Part3;
            IsParsed = true;
            m_ResolvedUUIDs.Add(this);

            return;
            g_InvalidUUID:
            throw new ArgumentException("Invalid uuid", "uuid");
        }

        public UUID(Byte[] a_UUID)
        {
            UUID_Part0 = BitConverter.ToInt16(a_UUID, 0);
            UUID_Part1 = BitConverter.ToInt16(a_UUID, 2);
            UUID_Part2 = BitConverter.ToInt64(a_UUID, 4);
            UUID_Part3 = BitConverter.ToInt32(a_UUID, 12);

            UUID_Part0_Bytes = BitConverter.GetBytes(UUID_Part0);
            UUID_Part1_Bytes = BitConverter.GetBytes(UUID_Part1);
            UUID_Part2_Bytes = BitConverter.GetBytes(UUID_Part2);
            UUID_Part3_Bytes = BitConverter.GetBytes(UUID_Part3);
            IsParsed = true;

            m_ResolvedUUIDs.Add(this);
        }

        public static Boolean IsResolved(String a_UUID)
        {
            foreach (String t_ResolvedUUID in m_ResolvedUUIDs)
                if (t_ResolvedUUID == a_UUID)
                    return true;

            return false;
        }

        public static void ForceResolve(String a_UUID)
        {
            if (!IsResolved(a_UUID))
            {
                Console.WriteLine("UUID force resolve for " + a_UUID);
                m_ResolvedUUIDs.Add(a_UUID);
            }
        }

        private void CreateUUID()
        {
            if (!IsParsed)
            {
                PartHelper = (Int16)(DateTime.Now.Millisecond | DateTime.Now.Second);
                UUID_Part0 = (Int16)(DateTime.Now.Year & PartHelper);
                UUID_Part1 = (Int16)(DateTime.Now.Month | DateTime.Now.Day & PartHelper);
                UUID_Part2 = DateTime.Now.Ticks | (UInt16)(PartHelper << 4);
                UUID_Part3 = DateTime.Now.Hour | DateTime.Now.Minute ^ DateTime.Now.Second & PartHelper;

                UUID_Part2 += m_UniqueHelper++;
            }

            UUID_Part0_Bytes = BitConverter.GetBytes(UUID_Part0);
            UUID_Part1_Bytes = BitConverter.GetBytes(UUID_Part1);
            UUID_Part2_Bytes = BitConverter.GetBytes(UUID_Part2);
            UUID_Part3_Bytes = BitConverter.GetBytes(UUID_Part3);

            if (Seed != 0)
            {
                if (Seed % 2 == 0)
                    for (int i = 0; i < UUID_Part0_Bytes.Length; i++)
                        UUID_Part0_Bytes[i] = (byte)(UUID_Part0_Bytes[i] ^ Seed);

                if (Seed % 3 == 0)
                    for (int i = 0; i < UUID_Part1_Bytes.Length; i++)
                        UUID_Part1_Bytes[i] = (byte)(UUID_Part1_Bytes[i] ^ Seed);

                if (Seed % 6 == 0)
                    for (int i = 0; i < UUID_Part2_Bytes.Length; i++)
                        UUID_Part2_Bytes[i] = (byte)(UUID_Part2_Bytes[i] ^ Seed);

                if (Seed % 12 == 0)
                    for (int i = 0; i < UUID_Part3_Bytes.Length; i++)
                        UUID_Part3_Bytes[i] = (byte)(UUID_Part3_Bytes[i] ^ Seed);
            }
        }

        public Byte[] ToBytes()
        {
            List<Byte> t_Bytes = new List<Byte>();
            t_Bytes.AddRange(UUID_Part0_Bytes);
            t_Bytes.AddRange(UUID_Part1_Bytes);
            t_Bytes.AddRange(UUID_Part2_Bytes);
            t_Bytes.AddRange(UUID_Part3_Bytes);
            return t_Bytes.ToArray();
        }

        public override string ToString()
        {
            return String.Format("{0}-{1}-{2}-{3}",
                BitConverter.ToString(UUID_Part0_Bytes).Replace("-", "").ToLower(),
                BitConverter.ToString(UUID_Part1_Bytes).Replace("-", "").ToLower(),
                BitConverter.ToString(UUID_Part2_Bytes).Replace("-", "").ToLower(),
                BitConverter.ToString(UUID_Part3_Bytes).Replace("-", "").ToLower());
        }
    }
}
