﻿using JSocket;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Hamal.Server.PacketProcessors
{
    public partial class PacketProcessor
    {
        public static JSONPacket ProcessChat(RemoteClient a_Sender, JSONServer a_Server, JSONPacket a_Packet)
        {
            Console.WriteLine("[CHAT] Processing packet from {0}...", a_Sender.ClientID);

            if (a_Packet.PacketName != "CHAT")
                return null;

            String t_Command = a_Packet.Data["COMMAND"].ToString().ToUpper();

            Console.WriteLine("[CHAT] Command: {0}", t_Command);

            switch (t_Command)
            {
                case "JOINCREATE":
                    Console.WriteLine("Joincreate");
                    String t_UID = null;
                    String t_Name = "New room #" + new Random().Next(10000, 99999);
                    String t_Password = "";
                    Boolean t_IsPrivate = false;

                    if (a_Packet.Data.ContainsKey("TARGET") && a_Packet.Data["TARGET"] != null)
                        t_UID = a_Packet.Data["TARGET"].ToString();

                    if (a_Packet.Data.ContainsKey("NAME") && a_Packet.Data["NAME"] != null)
                        t_Name = a_Packet.Data["NAME"].ToString();

                    if (a_Packet.Data.ContainsKey("PASSWORD") && a_Packet.Data["PASSWORD"] != null)
                        t_Password = a_Packet.Data["PASSWORD"].ToString().FromBase64();

                    if (a_Packet.Data.ContainsKey("PRIVATE"))
                        t_IsPrivate = a_Packet.Data["PRIVATE"] == (Object)1;

                    ChatRoom t_TargetRoom = Chat.GetRoom(t_UID);
                    ChatRoom t_AnswerRoom = null;

                    if (t_TargetRoom == null)
                    {
                        ChatMember t_ChatMember = new ChatMember();
                        t_ChatMember.DatabaseID = Helper.Clients[a_Sender].DatabaseID;
                        t_ChatMember.JoinTime = DateTime.Now;
                        t_ChatMember.Permissions =
                            ChatPermission.BAN |
                            ChatPermission.DELETE_MESSAGE_OTHERS |
                            ChatPermission.DELETE_MESSAGE_OWN |
                            ChatPermission.DELETE_ROOM |
                            ChatPermission.INVITE |
                            ChatPermission.KICK |
                            ChatPermission.READ |
                            ChatPermission.WRITE;
                        t_TargetRoom = Chat.CreateRoom(t_Name, new List<ChatMember>(new[] { t_ChatMember }));
                        t_TargetRoom.DefaultPermissions = ChatPermission.READ | ChatPermission.DELETE_MESSAGE_OWN | ChatPermission.WRITE;
                        t_TargetRoom.Password = t_Password;
                        t_TargetRoom.IsPrivate = t_IsPrivate;
                        Chat.UpdateRoom(t_TargetRoom);
                        t_AnswerRoom = t_TargetRoom;
                    }

                    if (t_AnswerRoom == null)
                    {
                        t_TargetRoom.Join(Helper.Clients[a_Sender].DatabaseID, t_TargetRoom.DefaultPermissions);
                        t_AnswerRoom = Chat.CopyRoom(t_TargetRoom);
                    }

                    Console.WriteLine("Joincreate end");

                    return new JSONPacket("CHAT", new Dictionary<String, Object>()
                    {
                        { "COMMAND", "JOINCREATE_ANSWER" },
                        { "STATE", "SUCCESS" },
                        { "ROOM", Chat.SerializeRoom(t_AnswerRoom) }
                    });
                case "GET_ROOMS_FOR_USER":
                    Int64 t_DBID = Convert.ToInt64(a_Packet.Data["USER_DBID"]);
                    List<ChatRoom> t_Rooms = Chat.GetChatRoomsForUser(t_DBID);

                    return new JSONPacket("CHAT", new Dictionary<String, Object>()
                    {
                        { "COMMAND", "GET_ROOMS_FOR_USER_ANSWER" },
                        { "STATE", t_Rooms == null ? "FAILED" : "SUCCESS" },
                        { "ROOMS", t_Rooms == null ? null : t_Rooms.Select(x => Chat.SerializeRoom(x, true)).ToArray() }
                    });
                case "ROOM_EXISTS":
                    t_UID = a_Packet.Data["ROOM_UID"].ToString();
                    return new JSONPacket("CHAT", new Dictionary<String, Object>()
                    {
                        { "COMMAND", "ROOM_EXISTS_ANSWER" },
                        { "STATE", "SUCCESS" },
                        { "EXISTS", Convert.ToInt32(Chat.DoesChatRoomExist(t_UID)) }
                    });
                case "GET_ROOM":
                    t_UID = a_Packet.Data["ROOM_UID"].ToString();
                    ChatRoom t_Room = Chat.GetRoom(t_UID);
                    return new JSONPacket("CHAT", new Dictionary<String, Object>()
                    {
                        { "COMMAND", "GET_ROOM_ANSWER" },
                        { "STATE", t_Room == null ? "FAILED" : "SUCCESS" },
                        { "ROOM", Chat.SerializeRoom(t_Room) }
                    });
                case "GET_ROOMS_BY_NAME":
                    t_Name = a_Packet.Data["ROOM_NAME"].ToString();
                    t_Rooms = Chat.GetRoomsByName(t_Name);

                    return new JSONPacket("CHAT", new Dictionary<String, Object>()
                    {
                        { "COMMAND", "GET_ROOMS_BY_NAME_ANSWER" },
                        { "STATE", t_Rooms == null ? "FAILED" : "SUCCESS" },
                        { "ROOMS", t_Rooms == null ? null : t_Rooms.Select(x => Chat.SerializeRoom(x)).ToArray() }
                    });
                case "UPDATE_ROOM":
                    t_Room = Chat.SerializeRoom((Dictionary<String, Object>)a_Packet.Data["ROOM_INFO"]);
                    Chat.UpdateRoom(t_Room);
                    break;
                case "LEAVE_ROORM":
                    Chat.GetRoom(a_Packet.Data["TARGET"].ToString()).Leave(Helper.GetUserDBID(a_Sender.ClientID));
                    break;
                case "SEND_MESSAGE":
                    t_UID = null;

                    if (a_Packet.Data.ContainsKey("TARGET") && a_Packet.Data["TARGET"] != null)
                        t_UID = a_Packet.Data["TARGET"].ToString();

                    t_TargetRoom = Chat.GetRoom(t_UID);

                    if (t_TargetRoom != null)
                        if (t_TargetRoom.HasUser(Helper.Clients[a_Sender].DatabaseID))
                        {
                            t_TargetRoom.SendMessage(a_Packet.Data["MESSAGE"].ToString().FromBase64(), Helper.Clients[a_Sender].DatabaseID);

                            foreach (ChatMember mem in t_TargetRoom.Members)
                                if (mem.Permissions.HasFlag(ChatPermission.READ))
                                    Helper.GetClientByDBID(mem.DatabaseID).Send(new JSONPacket("CHAT", new Dictionary<String, Object>()
                                    {
                                        { "COMMAND", "GOT_MESSAGE" },
                                        { "UID", t_TargetRoom.UID },
                                        { "HISTORY", t_TargetRoom.History.Select(x => Chat.SerializeChatEntry(x)).ToArray() }
                                    }));

                            Console.WriteLine("[CHAT] Message send from " + Helper.Clients[a_Sender].DatabaseID + " message: '" + a_Packet.Data["MESSAGE"].ToString().FromBase64() + "'");
                        }
                        else
                            return new JSONPacket("SHOW_ALERT", new Dictionary<String, Object>()
                            {
                                { "TITLE", "Invalid chat command" },
                                { "MESSAGE", "Cannot send a message to a room, wich the sender not member of" },
                                { "ICON", 48 },
                                { "UID", new UUID() }
                            });
                    break;

                default:
                    String t_Args = "";
                    foreach (KeyValuePair<String, Object> t_Pair in a_Packet.Data)
                        t_Args += t_Pair.Key + ": " + (t_Pair.Value as String) + "\n";

                    return new JSONPacket("SHOW_ALERT", new Dictionary<string, object>()
                        {
                            { "TITLE", "Cannot handle chat request" },
                            { "MESSAGE", String.Format("Failed to handle chat packet with command {0}. Arguments: {1}", t_Command, t_Args) },
                            { "ICON", 48 },
                            { "UID", new UUID() }
                        });
            }

            return null;
        }
    }
}
