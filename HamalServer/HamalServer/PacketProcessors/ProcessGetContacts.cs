﻿using JSocket;
using System;
using System.Collections.Generic;

namespace Hamal.Server.PacketProcessors
{
    public partial class PacketProcessor
    {
        public static void ProcessGetContacts(RemoteClient t_Sender, JSONServer t_Server, JSONPacket t_Packet)
        {
            Int64[] t_Contacts = Helper.GetContacts(Helper.Clients[t_Sender]);

            t_Sender.Send(new JSONPacket("CONTACTS_ANSWER", new Dictionary<String, Object>()
            {
                { "CONTACT_STATE", t_Contacts.Length == 0 ? 0 : 1 },
                { "CONTACTS", t_Contacts }
            }));
        }
    }
}
