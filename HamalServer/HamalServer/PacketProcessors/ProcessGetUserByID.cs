﻿using JSocket;
using System;
using System.Collections.Generic;

namespace Hamal.Server.PacketProcessors
{
    public partial class PacketProcessor
    {
        public static void ProcessGetUserByID(RemoteClient t_Sender, JSONServer t_Server, JSONPacket t_Packet)
        {
            Int64 t_UserDBID = Convert.ToInt64(t_Packet.Data["USER_DBID"]);
            AuthenticationInfo t_Info = Helper.GetUserByDBID(t_UserDBID);

            t_Sender.Send(new JSONPacket("USER_DATA_ANSWER", new Dictionary<string, object>()
            {
                { "USER_DBID", t_UserDBID },
                { "USER_INFO", new Dictionary<string, object>()
                    {
                        { "DISPLAY_NAME", t_Info.DisplayName.ToBase64() },
                        { "UUID", t_Info.UUID },
                        { "STATUS", t_Info.Status == UserStatus.HIDDEN ? UserStatus.OFFLINE : t_Info.Status },
                        { "STATUS_MESSAGE", t_Info.StatusMessage.ToBase64() }
                    }
                }
            }));
        }
    }
}
