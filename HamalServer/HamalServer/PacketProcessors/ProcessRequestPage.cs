﻿using JSocket;
using System;
using System.Collections.Generic;

namespace Hamal.Server.PacketProcessors
{
    public partial class PacketProcessor
    {
        public static void ProcessRequestPage(RemoteClient t_Sender, JSONServer t_Server, JSONPacket t_Packet)
        {
            if (Helper.Clients[t_Sender].State == AuthenticationState.LOGIN_OK)
            {
                t_Sender.Send(new JSONPacket("SET_PAGE", new Dictionary<String, Object>() { { "PAGE", t_Packet.Data["PAGE"].ToString() } }));
                Console.WriteLine("Switching client[{0}] to '{1}'.", t_Sender.ClientID, t_Packet.Data["PAGE"].ToString().ToLower().Replace('_', ' '));
            }
            else
            {
                t_Sender.Send(new JSONPacket("SET_PAGE", new Dictionary<String, Object>() { { "PAGE", "LOGIN_PAGE" } }));
                Console.WriteLine("Switching client[{0}] to 'login page'. [Reason: Authentication error]", t_Sender.ClientID);
            }
        }
    }
}
