﻿using DataStorage;
using JSocket;
using System;
using System.Collections.Generic;

namespace Hamal.Server.PacketProcessors
{
    public partial class PacketProcessor
    {
        public static void ProcessGetIcon(RemoteClient t_Sender, JSONServer t_Server, JSONPacket t_Packet)
        {
            Int64 t_UserDBID = Convert.ToInt64(t_Packet.Data["DBID"]);

            DataFile t_Data = Helper.GetUserData(t_UserDBID);

            if (t_Data != null)
                t_Sender.Send(new JSONPacket("ICON_INFO", new Dictionary<String, Object>()
                {
                    { "DBID", t_UserDBID },
                    { "ICON_BASE64", Convert.ToBase64String(t_Data.GetEntry("USER_DATA", "ProfilePicture")) }
                }));
        }
    }
}
