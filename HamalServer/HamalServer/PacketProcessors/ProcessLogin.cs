﻿using JSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hamal.Server.PacketProcessors
{
    public partial class PacketProcessor
    {
        public static void ProcessLogin(RemoteClient t_Sender, JSONServer t_Server, JSONPacket t_Packet)
        {
            AuthenticationInfo t_Info = Helper.Authenticate(t_Packet.Data["Username"].ToString().FromBase64(), t_Packet.Data["Password"].ToString().FromBase64());

            if (t_Info.State != AuthenticationState.LOGIN_OK)
            {
                t_Sender.Send(new JSONPacket("LOGIN_ANSWER", new Dictionary<string, object>()
                {
                    { "STATE", (Int32)t_Info.State }
                }));

                Thread.Sleep(400);
                t_Sender.TcpStream.Close();
                t_Sender.TcpClient.Close();
                Helper.Clients.Remove(t_Sender);
            }
            else
            {
                Helper.Clients[t_Sender] = t_Info;

                String t_StatusMessage = t_Info.StatusMessage.ToBase64();

                if (t_StatusMessage == null)
                    t_StatusMessage = String.Empty;

                t_Sender.Send(new JSONPacket("LOGIN_ANSWER", new Dictionary<String, Object>()
                {
                    { "STATE", 0 },
                    { "DATABASE_ID", t_Info.DatabaseID },
                    { "DISPLAY_NAME", t_Info.DisplayName.ToBase64() },
                    { "UUID", t_Info.UUID.ToString() },
                    { "STATUS", (Int32)t_Info.Status },
                    { "STATUS_MESSAGE", t_StatusMessage }
                }));

                Int64[] t_Contats = Helper.GetContacts(Helper.Clients[t_Sender]);

                foreach (Int64 t_Contact in t_Contats)
                {
                    RemoteClient t_Client = Helper.GetClientByDBID(t_Contact);

                    if (t_Client == null)
                        return;

                    t_Client.Send(new JSONPacket("USER_DATA_ANSWER", new Dictionary<string, object>()
                    {
                        { "USER_DBID", t_Info.DatabaseID },
                        { "USER_INFO", new Dictionary<string, object>()
                            {
                                { "DISPLAY_NAME", t_Info.DisplayName.ToBase64() },
                                { "UUID", t_Info.UUID },
                                { "STATUS", t_Info.Status == UserStatus.HIDDEN ? UserStatus.OFFLINE : t_Info.Status },
                                { "STATUS_MESSAGE", t_Info.StatusMessage.ToBase64() }
                            }
                        }
                    }));
                }
            }
        }
    }
}
