﻿using JSocket;
using System;
using System.Collections.Generic;

namespace Hamal.Server.PacketProcessors
{
    public partial class PacketProcessor
    {
        public static void ProcessGetDisplayName(RemoteClient t_Sender, JSONServer t_Server, JSONPacket t_Packet)
        {
            Int64 t_UserDBID = Convert.ToInt64(t_Packet.Data["DBID"]);

            t_Sender.Send(new JSONPacket("ICON_INFO", new Dictionary<String, Object>()
            {
                { "DBID", t_UserDBID },
                { "DISPLAY_NAME", Helper.GetDisplayName(t_UserDBID) }
            }));
        }
    }
}
