﻿using JSocket;
using System;
using System.Collections.Generic;

namespace Hamal.Server.PacketProcessors
{
    public partial class PacketProcessor
    {
        public static void ProcessSetStatusMessage(RemoteClient t_Sender, JSONServer t_Server, JSONPacket t_Packet)
        {
            Helper.Clients[t_Sender].StatusMessage = t_Packet.Data["NEW_STATE"].ToString().FromBase64();

            Int64[] t_Contacts = Helper.GetContacts(Helper.Clients[t_Sender]);
            
            AuthenticationInfo t_Info = Helper.Clients[t_Sender];
            
            Helper.SaveClient(t_Info);

            foreach (Int64 t_Contact in t_Contacts)
            {
                RemoteClient t_Client = Helper.GetClientByDBID(t_Contact);

                if (t_Client == null)
                    return;

                t_Client.Send(new JSONPacket("USER_DATA_ANSWER", new Dictionary<string, object>()
                {
                    { "USER_DBID", t_Info.DatabaseID },
                    { "USER_INFO", new Dictionary<string, object>()
                        {
                            { "DISPLAY_NAME", t_Info.DisplayName.ToBase64() },
                            { "UUID", t_Info.UUID },
                            { "STATUS", t_Info.Status == UserStatus.HIDDEN ? UserStatus.OFFLINE : t_Info.Status },
                            { "STATUS_MESSAGE", t_Info.StatusMessage.ToBase64() }
                        }
                    }
                }));
            }
        }
    }
}
