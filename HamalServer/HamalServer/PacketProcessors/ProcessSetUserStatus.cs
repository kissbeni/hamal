﻿using JSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hamal.Server.PacketProcessors
{
    public partial class PacketProcessor
    {
        public static void ProcessSetUserStatus(RemoteClient t_Sender, JSONServer t_Server, JSONPacket t_Packet)
        {
            UserStatus t_Status = (UserStatus)Enum.Parse(typeof(UserStatus), t_Packet.Data["NEW_STATE"].ToString());
            Helper.Clients[t_Sender].Status = t_Status;

            Int64[] t_Contacts = Helper.GetContacts(Helper.Clients[t_Sender]);

            AuthenticationInfo t_State = Helper.Clients[t_Sender];

            Helper.SaveClient(t_State);

            foreach (Int64 t_Contact in t_Contacts)
            {
                RemoteClient t_Client = Helper.GetClientByDBID(t_Contact);

                if (t_Client == null)
                    return;

                t_Client.Send(new JSONPacket("USER_DATA_ANSWER", new Dictionary<String, Object>()
                {
                    { "USER_DBID", t_State.DatabaseID },
                    { "USER_INFO", new Dictionary<String, Object>()
                        {
                            { "DISPLAY_NAME", t_State.DisplayName.ToBase64() },
                            { "UUID", t_State.UUID },
                            { "STATUS", t_State.Status == UserStatus.HIDDEN ? UserStatus.OFFLINE : t_State.Status },
                            { "STATUS_MESSAGE", t_State.StatusMessage.ToBase64() }
                        }
                    }
                }));
            }
        }
    }
}
