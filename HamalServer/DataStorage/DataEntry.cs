﻿using System;

namespace DataStorage
{
    public class DataEntry
    {
        public UInt32 Offset;
        public UInt32 Hash;
        public UInt32 Size;
        public Byte[] Data;
    }
}
