﻿using ICSharpCode.SharpZipLib.BZip2;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DataStorage
{
    public class DataFile : IDisposable
    {
        private Dictionary<UInt32, DataEntry> m_Data = new Dictionary<UInt32, DataEntry>();
        private UInt32 m_DataSegmentOffset = 0;
        private String m_ThisFile = "";
        private Byte m_CryptoByte;

        public DataFile(String a_File)
        {
            m_ThisFile = a_File;

            if (!File.Exists(a_File))
                ManagementHelper.CreateFile(a_File);

            Byte[] t_FileData = Uncompress(File.ReadAllBytes(a_File));

            using (MemoryStream t_MemoryStream = new MemoryStream(t_FileData))
            using (BinaryReader t_BinaryReader = new BinaryReader(t_MemoryStream))
                if (Encoding.ASCII.GetString(t_BinaryReader.ReadBytes(3)) == "HDF")
                {
                    UInt32 t_Version = t_BinaryReader.ReadUInt32();

                    m_CryptoByte = BitConverter.GetBytes(t_Version)[0];

                    if (t_Version == 0x0E7EBC30)
                    {
                        UInt32 t_EntryCount = t_BinaryReader.ReadUInt32();
                        m_DataSegmentOffset = t_EntryCount * 12 + 7 + 4 /* Random int32 before the data block, WHY? */;
                        UInt32 t_Buffer = 0;

                        for (UInt32 t_Index = 0; t_Index < t_EntryCount; t_Index++)
                        {
                            DataEntry t_Entry = new DataEntry();
                            t_Entry.Hash = Crypt(t_BinaryReader.ReadUInt32());
                            t_Entry.Offset = Crypt(t_BinaryReader.ReadUInt32());
                            t_Entry.Size = Crypt(t_BinaryReader.ReadUInt32());

                            Int64 t_CurrentPosition = t_BinaryReader.BaseStream.Position;

                            t_BinaryReader.BaseStream.Position = m_DataSegmentOffset + t_Entry.Offset;
                            t_Entry.Data = Crypt(t_BinaryReader.ReadBytes(Convert.ToInt32(t_Entry.Size)));

                            t_BinaryReader.BaseStream.Position = t_CurrentPosition;

                            t_Buffer += t_Entry.Size;

                            m_Data.Add(t_Entry.Hash, t_Entry);
                        }
                    }
                }
        }

        public void Save()
        {
            using (MemoryStream t_MemoryStream = new MemoryStream())
            {
                using (BinaryWriter t_BinaryWriter = new BinaryWriter(t_MemoryStream))
                {
                    t_BinaryWriter.Write(Encoding.ASCII.GetBytes("HDF"));
                    t_BinaryWriter.Write(0x0E7EBC30);

                    UInt32 t_EntryCount = (UInt32)m_Data.Count;
                    t_BinaryWriter.Write(t_EntryCount);

                    m_DataSegmentOffset = t_EntryCount * 12 + 7;
                    UInt32 t_Buffer = 0;
                    UInt32[] t_Hases = m_Data.Keys.ToArray();

                    for (UInt32 t_Index = 0; t_Index < t_EntryCount; t_Index++)
                    {
                        DataEntry t_Entry = m_Data[t_Hases[t_Index]];

                        t_Entry.Offset = t_Buffer;

                        t_BinaryWriter.Write(Crypt(t_Entry.Hash));
                        t_BinaryWriter.Write(Crypt(t_Entry.Offset));
                        t_BinaryWriter.Write(Crypt(t_Entry.Size));

                        t_Buffer += t_Entry.Size;
                    }

                    foreach (DataEntry t_Entry in m_Data.Values)
                        t_BinaryWriter.Write(Crypt(t_Entry.Data));
                }

                File.WriteAllBytes(m_ThisFile, Compress(t_MemoryStream.ToArray()));
            }
        }

        public Byte[] GetEntry(String a_Key, String a_Value)
        {
            UInt32 t_Hash = ManagementHelper.GetHash(a_Key, a_Value);

            if (!m_Data.ContainsKey(t_Hash))
                return new Byte[0];
            else
                return m_Data[t_Hash].Data;
        }

        public void AddEntry(String a_Key, String a_Value, Byte[] a_Data)
        {
            UInt32 a_Hash = ManagementHelper.GetHash(a_Key, a_Value);

            if (!this.m_Data.ContainsKey(a_Hash))
                this.m_Data.Add(a_Hash, new DataEntry() { Hash = a_Hash, Size = (UInt32)a_Data.Length, Data = a_Data });
            else
            {
                this.m_Data[a_Hash].Size = (UInt32)a_Data.Length;
                this.m_Data[a_Hash].Data = a_Data;
            }
        }

        private UInt32 Crypt(UInt32 a_Src)
        {
            return BitConverter.ToUInt32(Crypt(BitConverter.GetBytes(a_Src)), 0);
        }

        private Byte[] Crypt(Byte[] a_Src)
        {
            //for (Int32 i = 0; i < src.Length; i++)
            //    src[i] = Convert.ToByte(src[i] ^ 0x25);

            return a_Src;
        }

        internal static Byte[] Compress(Byte[] a_Src)
        {
            MemoryStream t_MemoryStream = new MemoryStream();
            BZip2OutputStream t_BZip2OutputStream = new BZip2OutputStream(t_MemoryStream);
            t_BZip2OutputStream.Write(a_Src, 0, a_Src.Length);
            t_BZip2OutputStream.Finalize();
            t_BZip2OutputStream.Close();
            Byte[] t_Return = t_MemoryStream.ToArray();
            t_MemoryStream.Close();
            t_BZip2OutputStream = null;
            t_MemoryStream = null;

            return t_Return;
        }

        internal static Byte[] Uncompress(Byte[] a_Src)
        {
            MemoryStream t_MemoryStream = new MemoryStream(a_Src);
            BZip2InputStream t_BZip2OutputStream = new BZip2InputStream(t_MemoryStream);
            Byte[] t_BytesUncompressed = new Byte[t_BZip2OutputStream.Length];
            t_BZip2OutputStream.Read(t_BytesUncompressed, 0, (Int32)t_BZip2OutputStream.Length);
            t_BZip2OutputStream.Close();
            t_MemoryStream.Close();
            t_BZip2OutputStream = null;
            t_MemoryStream = null;

            return t_BytesUncompressed;
        }

        public void Dispose()
        {
            m_Data.Clear();
            m_ThisFile = null;
        }
    }
}
