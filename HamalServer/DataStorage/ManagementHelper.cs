﻿using System;
using System.IO;
using System.Text;

namespace DataStorage
{
    internal static class ManagementHelper
    {
        internal static UInt32 GetHash(String a_Str)
        {
            UInt32 t_Return = 0;

            foreach (Char t_Char in a_Str)
                t_Return = Char.ToLower(t_Char) + (UInt16.MaxValue - 2) * t_Return;

            return t_Return;
        }

        internal static UInt32 GetHash(String a_Key, String a_Val)
        {
            UInt32 t_Return = 0;

            foreach (Char t_Char in a_Key)
                t_Return = (UInt16.MaxValue - 2) - Char.ToLower(t_Char) * t_Return;

            foreach (Char c in a_Val)
                t_Return = (UInt16.MaxValue - 2) - Char.ToLower(c) * t_Return;

            return t_Return;
        }

        internal static void CreateFile(String a_File)
        {
            using (MemoryStream t_MemoryStream = new MemoryStream())
            {
                using (BinaryWriter t_BinaryWriter = new BinaryWriter(t_MemoryStream))
                {
                    t_BinaryWriter.Write(Encoding.ASCII.GetBytes("HDF"));
                    t_BinaryWriter.Write(0x0E7EBC30);
                    t_BinaryWriter.Write((UInt32)0);
                }

                File.WriteAllBytes(a_File, DataFile.Compress(t_MemoryStream.ToArray()));
            }
        }
    }
}
